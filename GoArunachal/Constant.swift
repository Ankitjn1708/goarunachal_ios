//
//  Constant.swift
//  BaseProjectSwift
//

//
import UIKit

class Constant: NSObject {
  
   // BaseURL for Goarunachal
    static let BASE_URL : String = "http://goarunachal.com/";
    
//  static let BASE_URL : String = "http://typingwork.co/testdb/";
    static let bucketName:String = "goarunachalnew";

//
    static let SavedUserData : String = "savedUserData"
    static let SavedRideBookingInfo: String = "SavedRideBookingInfo"
    static let APPCOLOUR:UIColor =  UIColor(red: 78/255.0, green: 138/255.0, blue: 30/255.0, alpha: 1.0)
    static let PLACEHOLDERCOLOR:UIColor = UIColor(red: 149/255.0, green: 179/255.0, blue: 215/255.0, alpha: 1.0)
    
     static let FONT_System_Medium : NSString = "Medium";
    
    
}
