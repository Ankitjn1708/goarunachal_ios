//
//  NewFrogetPasswordViewController.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 11/19/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit

class NewFrogetPasswordViewController: UIViewController {
    @IBOutlet weak var newPasswordTxtFld: UITextField!
    @IBOutlet weak var confirmPasswordTxtFld: UITextField!
    @IBAction func submitBtnClicked(_ sender: Any) {
        if (self.newPasswordTxtFld.text?.isEmpty)! && (self.confirmPasswordTxtFld.text?.isEmpty)!{
            ModalController.showNegativeCustomAlertWith(title: "", msg: "Please enter your new password")
            
        }else if (self.newPasswordTxtFld.text?.count)! < 4 && (self.confirmPasswordTxtFld.text?.count)! < 4 {
            ModalController.showNegativeCustomAlertWith(title: "", msg: "password should have minimum 4 characters.")
            
        }else if self.newPasswordTxtFld.text != self.confirmPasswordTxtFld.text {
            ModalController.showNegativeCustomAlertWith(title: "", msg: "Password and confirm password are not match.")
        }
        else{
            self.ForgetPasswordAPI()
        }
    }
    var apiStatus: String? = ""
    var previosMobileNumber: String? = ""

    var responseFinalDict : NSDictionary = NSDictionary()

    @IBOutlet weak var submitBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
self .makeNavigationButton()
        self.navigationController?.isNavigationBarHidden = false

        // Do any additional setup after loading the view.
        newPasswordTxtFld.attributedPlaceholder = NSAttributedString(string: "Enter new password",
                                                           attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 84.0/255.0, green: 84.0/255.0, blue: 84.0/255.0, alpha: 1.0)])
        newPasswordTxtFld.keyboardType = .default
        
        confirmPasswordTxtFld.attributedPlaceholder = NSAttributedString(string: "Enter confirm password",
                                                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 84.0/255.0, green: 84.0/255.0, blue: 84.0/255.0, alpha: 1.0)])
        confirmPasswordTxtFld.keyboardType = .default
        
        self.submitBtn.layer.cornerRadius = self.submitBtn.bounds.size.height/2
        self.submitBtn.clipsToBounds = true
        
        self.newPasswordTxtFld.isSecureTextEntry = true
        self.confirmPasswordTxtFld.isSecureTextEntry = true

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func makeNavigationButton()
    {
        navigationController?.navigationBar.barTintColor = Constant.APPCOLOUR
        
        self.navigationItem.title = "Forget Password"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let btn = UIButton(type: .custom)
        
        btn.widthAnchor.constraint(equalToConstant: 12).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        btn.setImage(UIImage(named: "back_icon.png"), for: .normal)
        
        btn.addTarget(self, action: #selector(backBtnClicked(btn:)), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btn)
        self.navigationItem.leftBarButtonItem = item
    }
    @objc func backBtnClicked(btn : UIButton)
    {
        _ = navigationController?.popViewController(animated: true)
        
    }
    
    func ForgetPasswordAPI()
    {
//        let userData = ModalController.getTheContentForKey(Constant.SavedUserData)
//
//        let mobileNumber: String = userData?.object(forKey: "mobile") as! String
//
        ModalClass.startLoading(self.view)
        let str = "\(Constant.BASE_URL)forgetpassword.php"
        
        let  parasm = "mobile=\(previosMobileNumber!)&newpass=\(self.newPasswordTxtFld.text!)&confirmnewpass=\(self.confirmPasswordTxtFld.text!)"
        print("request -",parasm)
        
        WebModel.makePostRequest(str, withParameters: parasm) { (response, success) in
            ModalClass.stopLoading()
            
            if success == true {
                // api hit success
                print (response ?? "N/A")
                let ResponseDict : NSDictionary = (response as? NSDictionary)!
                print("responseDict %@",ResponseDict)
                
                //                self.apiStatus = ""
                if let result =  (ResponseDict.object(forKey: "status") as? String)
                {
                    self.apiStatus = result
                }
                if let result =  (ResponseDict.object(forKey: "status") as? NSNumber)        {
                    self.apiStatus = String(describing: result)
                }
                
                let x = self.apiStatus
                if !(x == "1"){
                    
                }else{
                    self.responseFinalDict = (ResponseDict.object(forKey: "UserDetail") as! NSDictionary)
                    print("userDeta response \(self.responseFinalDict)")
                    ModalController.showAlertWith(message: "Password updated.", title: "")
//                    let dict : NSDictionary = NSDictionary(dictionary: self.responseFinalDict).RemoveNullValueFromDic()
//
//                    ModalController.saveTheContent(dict as? AnyObject, WithKey:Constant.SavedUserData)

                    let controllerArr = self.navigationController?.viewControllers as! [UIViewController]
                    for view in controllerArr
                    {
                        if view is LoginViewController
                        {
                            self.navigationController?.popToViewController(view, animated: true)
                            break;
                        }
                    }
                }
            }
            else
            {
                if response == nil
                {
                    print ("connection error")
                    ModalController.showNegativeCustomAlertWith(title: "connection error", msg: "")
                }else{
                    print ("data not in proper json")
                }
            }
        }
    }
}
