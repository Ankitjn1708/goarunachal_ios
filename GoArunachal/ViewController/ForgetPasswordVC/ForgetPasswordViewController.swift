//
//  ForgetPasswordViewController.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 11/19/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: UIViewController {
    @IBOutlet weak var textfld: UITextField!
    @IBOutlet weak var lowerlbl: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBAction func submitBtnAction(_ sender: Any) {
        view.endEditing(true)

            if (self.textfld.text?.isEmpty)!{
                ModalController.showNegativeCustomAlertWith(title: "", msg: "Please enter your mobile number.")
                
        }else if (self.textfld.text?.count)! < 10  {
                ModalController.showNegativeCustomAlertWith(title: "", msg: "Please enter a valid phone number.")
                
            }else if (self.textfld.text?.count)! > 12 {
                ModalController.showNegativeCustomAlertWith(title: "", msg: "Please enter a valid phone number.")
                
            }
            else{
                self.ForgetPasswordAPI()
            }
    }
    var apiStatus: String? = ""
    var responseFinalDict : NSDictionary = NSDictionary()

    @IBOutlet weak var headerLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false

        // Do any additional setup after loading the view.
        self.headerLbl.text = "Mobile No."
        textfld.attributedPlaceholder = NSAttributedString(string: "Mobile No.",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 84.0/255.0, green: 84.0/255.0, blue: 84.0/255.0, alpha: 1.0)])
        textfld.keyboardType = .numberPad
//        let userData = ModalController.getTheContentForKey(Constant.SavedUserData)
//        textfld.text = userData?.object(forKey: "mobile") as? String
        
        self.submitBtn.layer.cornerRadius = self.submitBtn.bounds.size.height/2
        self.submitBtn.clipsToBounds = true
        self .makeNavigationButton()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func makeNavigationButton()
    {
        navigationController?.navigationBar.barTintColor = Constant.APPCOLOUR
        
        self.navigationItem.title = "Forget Password"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let btn = UIButton(type: .custom)
        
        btn.widthAnchor.constraint(equalToConstant: 12).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        btn.setImage(UIImage(named: "back_icon.png"), for: .normal)
        
        btn.addTarget(self, action: #selector(backBtnClicked(btn:)), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btn)
        self.navigationItem.leftBarButtonItem = item
    }
    @objc func backBtnClicked(btn : UIButton)
    {
        _ = navigationController?.popViewController(animated: true)
        
    }
    func ForgetPasswordAPI()
    {
        ModalClass.startLoading(self.view)
        let str = "\(Constant.BASE_URL)forgetpassword.php"
        
          let  parasm = "mobile=\(self.textfld.text!)"
            print("request -",parasm)
            
       
        WebModel.makePostRequest(str, withParameters: parasm) { (response, success) in
            ModalClass.stopLoading()
            
            if success == true {
                // api hit success
                print (response ?? "N/A")
                let ResponseDict : NSDictionary = (response as? NSDictionary)!
                print("responseDict %@",ResponseDict)
                
                //                self.apiStatus = ""
                if let result =  (ResponseDict.object(forKey: "status") as? String)
                {
                    self.apiStatus = result
                }
                if let result =  (ResponseDict.object(forKey: "status") as? NSNumber)        {
                    self.apiStatus = String(describing: result)
                }
                
                let x = self.apiStatus
                if !(x == "1"){
                    ModalController.showNegativeCustomAlertWith(title: "", msg: "Your mobile number is not exist.")
                }else{
                    
                    let newViewController = NewFrogetPasswordViewController()
//                    newViewController.tripModal = self.tripModal;
                    newViewController.previosMobileNumber = self.textfld.text!
                    self.navigationController?.pushViewController(newViewController, animated: true)
                    
//                    self.responseFinalDict = (ResponseDict.object(forKey: "UserDetail") as! NSDictionary)
//                    print("userDeta response \(self.responseFinalDict)")
//                    ModalController.showAlertWith(message: "Profile updated.", title: "")
//                    let dict : NSDictionary = NSDictionary(dictionary: self.responseFinalDict).RemoveNullValueFromDic()
//
//                    ModalController.saveTheContent(dict as? AnyObject, WithKey:Constant.SavedUserData)
//
////                    self.delegate?.editSuccessfull()
//                    _ = self.navigationController?.popViewController(animated: true)
                }
            }
            else
            {
                if response == nil
                {
                    print ("connection error")
                    ModalController.showNegativeCustomAlertWith(title: "connection error", msg: "")
                }else{
                    print ("data not in proper json")
                }
            }
        }
    }
}
