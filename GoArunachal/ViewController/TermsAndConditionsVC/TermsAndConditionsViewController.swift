//
//  TermsAndConditionsViewController.swift
//  Scene Ahead
//
//  Created by aishwarya sanganan on 27/11/17.
//  Copyright © 2017 aishwarya sanganan. All rights reserved.
//

import UIKit

class TermsAndConditionsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    var apiStatus: String? = ""

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!
    var policyArray = NSArray()

    @IBOutlet weak var topLbl: UIImageView!
    var responseFinalDict : NSDictionary = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
//        if #available(iOS 11.0, *) {
//            let window = UIApplication.shared.keyWindow
//            let topPadding = window?.safeAreaInsets.top
//            let myIntValue = Int(topPadding!)
//
//            if myIntValue > 0{
//                self.topViewHeightConstraint.constant = 88
//            }
//        }
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 125
        
        self.termsAndConditionsApiGET()
        tableView.register(UINib(nibName: "TermsConditionsTableViewCell", bundle: nil), forCellReuseIdentifier: "TermsConditionsTableViewCell");
        
        tableView.separatorStyle = .none;
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.makeNavigationButton()

//        if #available(iOS 11.0, *) {
//            tableView.contentInsetAdjustmentBehavior = .never
//        } else {
//            // Fallback on earlier versions
//            let leftConstraint = NSLayoutConstraint(item: self.topLbl,
//                                                    attribute: NSLayoutConstraint.Attribute.top,
//                                                    relatedBy: NSLayoutConstraint.Relation.equal,
//                                                    toItem: self.view,
//                                                    attribute: NSLayoutConstraint.Attribute.top,
//                                                    multiplier: 1.0,
//                                                    constant: 31);
//            leftConstraint.isActive = true
//        }
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func makeNavigationButton()
    {
        navigationController?.navigationBar.barTintColor = Constant.APPCOLOUR
        
        self.navigationItem.title = "Terms & Conditions"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let btn = UIButton(type: .custom)
        
        btn.widthAnchor.constraint(equalToConstant: 12).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        btn.setImage(UIImage(named: "back_icon.png"), for: .normal)
        
        btn.addTarget(self, action: #selector(backBtnClicked(btn:)), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btn)
        self.navigationItem.leftBarButtonItem = item
    }
    @objc func backBtnClicked(btn : UIButton)
    {
        _ = navigationController?.popViewController(animated: true)
        
    }
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
            return 2
    }
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
                let cell = tableView.dequeueReusableCell(withIdentifier: "TermsConditionsTableViewCell",for: indexPath) as! TermsConditionsTableViewCell
                cell.selectionStyle = .none
        
        if indexPath.row == 0 {
            cell.conditionLbl.text = "TERMS OF SERVICE"
            if self.policyArray.count > 0{
               cell.mainLbl.text = (self.policyArray[indexPath.row] as AnyObject).value(forKey: "text") as? String
            }
            
           
        }else{
            cell.conditionLbl.text = "PRIVACY POLICY"
            if self.policyArray.count > 0{
                cell.mainLbl.text = (self.policyArray[indexPath.row] as AnyObject).value(forKey: "text") as? String
            }
        }
        
                return cell
    }
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    func rightButtonAction(sender: UIBarButtonItem)
    {
    }
    
    @IBAction func crossBtn(_ sender: Any) {
         _ = navigationController?.popViewController(animated: true)
    }
    func termsAndConditionsApiGET()
    {
        ModalClass.startLoading(self.view)
        let str = "\(Constant.BASE_URL)privacy.php"
        
        WebModel.makeGetRequest(str) { (response, success) in
            ModalClass.stopLoading()
            if success == true {
                // api hit success
                print (response ?? "N/A")
                self.responseFinalDict = (response as? NSDictionary)!
                print("ResponseDictionary %@",self.responseFinalDict)
                
                self.apiStatus = ""
                if let result =  (self.responseFinalDict.object(forKey: "Status") as? String)
                {
                    self.apiStatus = result
                }
                if let result =  (self.responseFinalDict.object(forKey: "Status") as? NSNumber)        {
                    self.apiStatus = String(describing: result)
                }
                
                let x = self.apiStatus
                if !(x == "1"){

                }else{
                    self.policyArray = (self.responseFinalDict.object(forKey: "PrivacyContent") as! NSArray)
                    print("T and C response \(self.policyArray)")
                    self.tableView.reloadData()
                }
            }
            else
            {
                if response == nil
                {
                    print ("connection error")
                    ModalController.showNegativeCustomAlertWith(title: "connection error", msg: "")
                }else{
                    print ("data not in proper json")
                }
            }
        }
    }

    }

