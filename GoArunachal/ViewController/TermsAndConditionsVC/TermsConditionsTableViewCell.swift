//
//  TermsConditionsTableViewCell.swift
//  Scene Ahead
//
//  Created by aishwarya sanganan on 27/11/17.
//  Copyright © 2017 aishwarya sanganan. All rights reserved.
//

import UIKit

class TermsConditionsTableViewCell: UITableViewCell {

    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var conditionLbl: UILabel!
    
    @IBOutlet weak var mainLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
