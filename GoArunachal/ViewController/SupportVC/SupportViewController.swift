//
//  SupportViewController.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/23/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit

class SupportViewController: UIViewController {
    @IBOutlet weak var supportLabel: UILabel!
    
    @IBOutlet weak var contactinfoLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.makeNavigationButton()
self.supportLabel.text = "Office near bank Tinali, \nItanagar opposite Head Post Office, \nlandmark Doni Polo B.Ed college Arunachal pradesh, \n\nemail. goarunachal7@gmail.com \ncontact 9402621722"
        // Do any additional setup after loading the view.
    }
//Office near bank Tinali, Itanagar opposite Head Post Office, landmark Doni Polo B.Ed college

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func makeNavigationButton()
    {
        navigationController?.navigationBar.barTintColor = Constant.APPCOLOUR
        
        self.navigationItem.title = "Support"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let btn = UIButton(type: .custom)
        
        btn.widthAnchor.constraint(equalToConstant: 12).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        btn.setImage(UIImage(named: "back_icon.png"), for: .normal)
        
        btn.addTarget(self, action: #selector(backBtnClicked(btn:)), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btn)
        self.navigationItem.leftBarButtonItem = item
    }
    @objc func backBtnClicked(btn : UIButton)
    {
        _ = navigationController?.popViewController(animated: true)
        
    }
}
