//
//  ProfileSecondTableViewCell.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/23/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit

class ProfileSecondTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var headerName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
