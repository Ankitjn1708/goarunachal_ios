//
//  MyProfileViewController.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/15/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
import AWSCore
import AWSS3

class MyProfileViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,updateProfileDelegate,updatePhoneNumberDelegate,editProfileImageDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate  {
    
    
    var headeraName: String? = ""
    var responseFinalDict : NSDictionary = NSDictionary()
    var responseDict : NSDictionary = NSDictionary()
    var modalDict : NSDictionary = NSDictionary()
    var apiStatus: String? = ""
    var mobileNumber: String? = ""
    var password: String? = ""
    
    let picker = UIImagePickerController()
    var tempImage: UIImage!
    var selectedImage: UIImage!
    var s3URL:String!
    var isProfileImageChanged : Bool = false

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true);
      self.tableView.separatorStyle = .none
      self.makeNavigationButton()
        // Do any additional setup after loading the view.
         tableView.register(UINib(nibName: "ProfileFirstTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileFirstTableViewCell");
          tableView.register(UINib(nibName: "ProfileSecondTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileSecondTableViewCell");
        self.UserProfileAPI()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func makeNavigationButton()
    {
        navigationController?.navigationBar.barTintColor = Constant.APPCOLOUR
        
        self.navigationItem.title = "Profile"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

        let btn = UIButton(type: .custom)
        
        btn.widthAnchor.constraint(equalToConstant: 12).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        btn.setImage(UIImage(named: "back_icon.png"), for: .normal)
        
        btn.addTarget(self, action: #selector(backBtnClicked(btn:)), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btn)
        self.navigationItem.leftBarButtonItem = item
    }
    @objc func backBtnClicked(btn : UIButton)
    {
        _ = navigationController?.popViewController(animated: true)

    }
    //MARK:- UItableViewDelegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileFirstTableViewCell",for: indexPath) as! ProfileFirstTableViewCell
            cell.selectionStyle = .none
            cell.emaillabel.text = responseFinalDict.object(forKey: "email") as? String
            cell.profilePicImg.layer.cornerRadius = cell.profilePicImg.layer.frame.size.width / 2
            cell.profilePicImg.clipsToBounds = true
            cell.profilePicImg.layer.borderColor = UIColor.white.cgColor
            cell.profilePicImg.layer.borderWidth = 1.0
            if self.isProfileImageChanged{
                self.isProfileImageChanged = false
            }else{
                
                 var myPic  = responseFinalDict.object(forKey: "imgurl") as? AnyObject
                
                if !(myPic is NSNull)
                {
                    myPic = myPic?.replacingOccurrences(of: " "
                        , with: "%20") as AnyObject
                    cell.profilePicImg.sd_setImage(with: URL(string: myPic as! String), placeholderImage: UIImage(named: "boy.png"))
                }
                else{
                cell.profilePicImg.image = UIImage(named: "boy.png")
                }
            }
            cell.delegate = self
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileSecondTableViewCell",for: indexPath) as! ProfileSecondTableViewCell
            cell.selectionStyle = .none
            cell.headerName.text = "Name"
            cell.nameLabel.text = responseFinalDict.object(forKey: "name") as? String
            
//            cell.delegate = self
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileSecondTableViewCell",for: indexPath) as! ProfileSecondTableViewCell
            cell.selectionStyle = .none
            cell.headerName.text = "Phone Number"
            if let result =  (responseFinalDict.object(forKey: "mobile") as? String)
            {
                self.mobileNumber = result
            }
            if let result =  (responseFinalDict.object(forKey: "mobile") as? NSNumber)        {
                self.mobileNumber = String(describing: result)
            }
           cell.nameLabel.text = self.mobileNumber
            
//            cell.delegate = self
            cell.tag = indexPath.row
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileSecondTableViewCell",for: indexPath) as! ProfileSecondTableViewCell
            cell.selectionStyle = .none
            cell.headerName.text = "Password"

            if let result =  (responseFinalDict.object(forKey: "password") as? String)
            {
                self.password = result
            }
            if let result =  (responseFinalDict.object(forKey: "password") as? NSNumber)        {
                self.password = String(describing: result)
            }
            cell.nameLabel.text = self.password
            
//            cell.delegate = self
            return cell
        }

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
                return 200
            
        }else if indexPath.row == 1 {
            return 75
            
        }else if indexPath.row == 2 {
            return 75
        }else{
            return 75
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            
        }else if indexPath.row == 1 {
            let newViewController = EditProfileViewController()
            newViewController.selectedScreen = 1
            newViewController.delegate = self
            self.navigationController?.pushViewController(newViewController, animated: true)
        }else if indexPath.row == 2 {
            let newViewController = EditProfileViewController()
            newViewController.selectedScreen = 2
            newViewController.delegate = self
            self.navigationController?.pushViewController(newViewController, animated: true)
        }else{
            let newViewController = EditPhoneNumberViewController()
            newViewController.delegate = self
            self.navigationController?.pushViewController(newViewController, animated: true)
            
        }
    }
    
    func UserProfileAPI()
    {
        ModalClass.startLoading(self.view)
        let str = "\(Constant.BASE_URL)viewprofile.php"
        
        let parasm : String = "id=\((ModalController.getTheContentForKey("userID") as AnyObject))"
        print("request -",parasm)
        
        WebModel.makePostRequest(str, withParameters: parasm) { (response, success) in
            ModalClass.stopLoading()
            
            if success == true {
                // api hit success
                print (response ?? "N/A")
                let ResponseDict : NSDictionary = (response as? NSDictionary)!
                print("responseDict %@",ResponseDict)
                
//                self.apiStatus = ""
                if let result =  (ResponseDict.object(forKey: "status") as? String)
                {
                    self.apiStatus = result
                }
                if let result =  (ResponseDict.object(forKey: "status") as? NSNumber)        {
                    self.apiStatus = String(describing: result)
                }
                
                let x = self.apiStatus
                if !(x == "1"){
                    
                }else{
                    self.responseFinalDict = (ResponseDict.object(forKey: "UserDetail") as! NSDictionary)
                    print("userDeta response \(self.responseFinalDict)")

                    self.tableView.reloadData()
                }
            }
            else
            {
                if response == nil
                {
                    print ("connection error")
                    ModalController.showNegativeCustomAlertWith(title: "connection error", msg: "")
                }else{
                    print ("data not in proper json")
                }
            }
            }
        }
    func editSuccessfull() {

        self .UserProfileAPI()
    }
    func editPhoneNumberSuccessfull()
    {
        self.UserProfileAPI()
    }
    func editProfileImageSuccessfull() {
        let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default , handler:{ (UIAlertAction)in
            print("User click take photo")
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.picker.allowsEditing = false
                self.picker.delegate = self
                self.picker.sourceType = UIImagePickerController.SourceType.camera
                self.picker.cameraCaptureMode = .photo
                self.picker.modalPresentationStyle = .fullScreen
                self.present(self.picker,animated: true,completion: nil)
            }else {
                self.noCamera()
            }
        }))
        alert.addAction(UIAlertAction(title: "Pick from gallery", style: .default , handler:{ (UIAlertAction)in
            print("User click take video")
            print("Choose photo from gallery")
            self.picker.allowsEditing = false
            self.picker.sourceType = .photoLibrary
            self.picker.delegate = self
            self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.picker.modalPresentationStyle = .popover
            self.present(self.picker,animated: true,completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
//MARK: - Delegates image picker
func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage]
            as? UIImage else {
                return
        }
        var  chosenImage = UIImage()
        chosenImage = image

        self.selectedImage = chosenImage
        self.tableView.reloadData()
    
    if chosenImage.size.width > 414
    {
        let factor = chosenImage.size.width / 414
        chosenImage =  chosenImage.scaleImageToSize(newSize: CGSize(width: 414, height: chosenImage.size.height/factor))
    }else if chosenImage.size.height > 812
    {
        let factor = chosenImage.size.height / 812
        chosenImage =  chosenImage.scaleImageToSize(newSize: CGSize(width: chosenImage.size.width/factor, height: 812))
    }
    
        self.uploadImageToAWS(img: chosenImage)
        dismiss(animated:true, completion: nil) //5
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.picker.dismiss(animated: true) {() -> Void in }
    }
    //MARK: - aws methods
    func uploadImageToAWS(img:UIImage)
    {
        ModalClass.startLoading(self.view)
        // getting local path
        var imageURL = NSURL()
        let imageName:String! = "goarunachal";
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! as String
        let localPath = (documentDirectory as NSString).appendingPathComponent(imageName!)
        
        let data = img.jpegData(compressionQuality: 0.1)
        do
        {
            if data != nil {
                try data?.write(to: URL(fileURLWithPath: localPath), options: .atomic)
            }else{
                return;
            }
        }catch{
            print(error)
        }
        imageURL = NSURL(fileURLWithPath: localPath)
        let S3BucketName: String = Constant.bucketName;
        var _: AWSS3TransferUtilityUploadCompletionHandlerBlock?
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.uploadProgress = {(bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) -> Void in
            DispatchQueue.main.async(execute: {() -> Void in
                //Update progress
            })
        }
        let key : String = NSString(format: "profile_image_%@.png",(String(round(NSDate().timeIntervalSince1970) * 1000).replacingOccurrences(of: ".", with: ""))) as String
        print(key)
        uploadRequest?.body = imageURL as URL
        uploadRequest?.key = key
        uploadRequest?.bucket = S3BucketName
        // uploadRequest?.contentType = "image/" + ".png"
        uploadRequest?.contentType = "image/" + ".png"
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith { (task) -> AnyObject! in
            if let error = task.error {
                DispatchQueue.main.async {
                    ModalClass.stopLoading()
                }
                print("Upload failed ❌ (\(error))");
                let banner = Banner(title: "Upload failed. Please try again.", subtitle: "", image: UIImage(named: ""), backgroundColor: UIColor.red)
                banner.dismissesOnTap = true
                banner.show(duration: 1.5)
            }else{
                DispatchQueue.main.async {
                    ModalClass.stopLoading()
                }
                if task.result != nil
                {
                    self.s3URL = "https://s3.ap-south-1.amazonaws.com/\(S3BucketName)/\(key)";
                    // self.callWebServicesAddPost("");
                    print("Uploaded to:\n\(self.s3URL!)")
                    DispatchQueue.main.async {
                          self.isProfileImageChanged = true
                       self .UpdateProfileImageAPI()
                    }
                }
                else
                {
                    print("Unexpected empty result.")
                }
                
            }
            return nil
        }
    }
    func UpdateProfileImageAPI()
    {
        ModalClass.startLoading(self.view)
        let str = "\(Constant.BASE_URL)updateprofile.php"
       
         let  parasm = "imgurl=\(self.s3URL!)&id=\((ModalController.getTheContentForKey("userID") as AnyObject))"
            print("request -",parasm)
        
        WebModel.makePostRequest(str, withParameters: parasm) { (response, success) in
            ModalClass.stopLoading()
            
            if success == true {
                // api hit success
                print (response ?? "N/A")
                let ResponseDict : NSDictionary = (response as? NSDictionary)!
                print("responseDict %@",ResponseDict)
                
                //                self.apiStatus = ""
                if let result =  (ResponseDict.object(forKey: "status") as? String)
                {
                    self.apiStatus = result
                }
                if let result =  (ResponseDict.object(forKey: "status") as? NSNumber)        {
                    self.apiStatus = String(describing: result)
                }
                
                let x = self.apiStatus
                if !(x == "1"){
                    
                }else{
                   
                    self.responseDict = (ResponseDict.object(forKey: "UserDetail") as! NSDictionary)
                    print("userDeta response \(self.responseDict)")
                     ModalController.showAlertWith(message: "Profile Picture added.", title: "")
                    let dict : NSDictionary = NSDictionary(dictionary: self.responseDict).RemoveNullValueFromDic()
                    
                    ModalController.saveTheContent(dict as? AnyObject, WithKey:Constant.SavedUserData)
                    
                    self.UserProfileAPI()
                    self.tableView.reloadData()
                }
            }
            else
            {
                if response == nil
                {
                    print ("connection error")
                    ModalController.showNegativeCustomAlertWith(title: "connection error", msg: "")
                }else{
                    print ("data not in proper json")
                }
            }
        }
    }

    
}
