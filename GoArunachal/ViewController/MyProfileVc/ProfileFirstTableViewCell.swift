//
//  ProfileFirstTableViewCell.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/23/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
protocol editProfileImageDelegate: class {
    func editProfileImageSuccessfull()
}
class ProfileFirstTableViewCell: UITableViewCell {
    @IBOutlet weak var emaillabel: UILabel!
    weak var delegate: editProfileImageDelegate?

    @IBAction func editBtnClicked(_ sender: Any) {
        self.delegate?.editProfileImageSuccessfull()
    }
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var profilePicImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
