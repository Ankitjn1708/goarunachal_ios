//
//  AddPlaceSecondTableViewCell.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/26/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
protocol AddPlaceSecondTableViewCellDelegate: class {
    func ImageSelectedClicked()
    
}
class AddPlaceSecondTableViewCell: UITableViewCell {
    @IBOutlet weak var placeImageView: UIImageView!
    weak var delegate: AddPlaceSecondTableViewCellDelegate?
    @IBAction func imageSelectClicked(_ sender: Any) {
        self.delegate?.ImageSelectedClicked()
    }
    
    @IBOutlet weak var imageSelected: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
