//
//  AddPlaceFirstTableViewCell.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/26/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
protocol AddPlaceFirstTableViewCellDelegate: class {
    func entryPlaceName(entryNameStr: String, _ sender: Any)
}
class AddPlaceFirstTableViewCell: UITableViewCell,UITextFieldDelegate {
    @IBAction func entryFieldClicked(_ sender: Any) {
        self.delegate?.entryPlaceName(entryNameStr: self.entryFieldTxtFld.text!, self)
    }
    weak var delegate: AddPlaceFirstTableViewCellDelegate?
    @IBOutlet weak var entryFieldTxtFld: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let paddingViw = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 45))
        self.entryFieldTxtFld.leftView = paddingViw
        self.entryFieldTxtFld.leftViewMode = .always
        self.entryFieldTxtFld.delegate = self as UITextFieldDelegate
        
        self.entryFieldTxtFld.layer.borderWidth = 2.0
        self.entryFieldTxtFld.layer.borderColor = (UIColor(red: 218 / 255.0, green: 217 / 255.0, blue: 218 / 255.0, alpha: 1.0)).cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if self.tag == 3{
            
            let MAX_LENGTH = 10
            
            let newLength: Int = (self.entryFieldTxtFld.text?.count)! + string.count - range.length
            if newLength > MAX_LENGTH {
                return false
            } else {
                return true
            }
            
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
}
