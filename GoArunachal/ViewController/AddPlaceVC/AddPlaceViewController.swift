//
//  AddPlaceViewController.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/26/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
import AWSCore
import AWSS3


class AddPlaceViewController: UIViewController,AddPlaceSecondTableViewCellDelegate,AddPlaceThirdTableViewCellDelegate,UITableViewDelegate,UITableViewDataSource,AddPlaceFirstTableViewCellDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var tableView: UITableView!
    var addPLaceName: String? = ""
    var apiStatusStr: String? = ""
    let picker = UIImagePickerController()
    var   tempImage: UIImage!
    var selectedImage: UIImage!
    var FinalApiResponseDict : NSDictionary = NSDictionary()
    var s3URL:String!
    override func viewDidLoad() {
        super.viewDidLoad()
     self .makeNavigationButton()
        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: "AddPlaceFirstTableViewCell", bundle: nil), forCellReuseIdentifier: "AddPlaceFirstTableViewCell");
        tableView.register(UINib(nibName: "AddPlaceSecondTableViewCell", bundle: nil), forCellReuseIdentifier: "AddPlaceSecondTableViewCell");
        tableView.register(UINib(nibName: "AddPlaceThirdTableViewCell", bundle: nil), forCellReuseIdentifier: "AddPlaceThirdTableViewCell");
        self.selectedImage = UIImage(named: "image_upload_placeholder.png")
        self.tableView.separatorStyle = .none
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func makeNavigationButton()
    {
        navigationController?.navigationBar.barTintColor = Constant.APPCOLOUR
        
        self.navigationItem.title = "Add Place"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let btn = UIButton(type: .custom)
        
        btn.widthAnchor.constraint(equalToConstant: 12).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        btn.setImage(UIImage(named: "back_icon.png"), for: .normal)
        
        btn.addTarget(self, action: #selector(backBtnClicked(btn:)), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btn)
        self.navigationItem.leftBarButtonItem = item
    }
    @objc func backBtnClicked(btn : UIButton)
    {
        _ = navigationController?.popViewController(animated: true)
        
    }
    // MARK:- UITabelViewDelegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddPlaceFirstTableViewCell",for: indexPath) as! AddPlaceFirstTableViewCell
            cell.selectionStyle = .none
            cell.entryFieldTxtFld.layer.cornerRadius = 4
            cell.entryFieldTxtFld.clipsToBounds = true;
            cell.entryFieldTxtFld.text = addPLaceName

            cell.delegate = self
            cell.tag = indexPath.row
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddPlaceSecondTableViewCell",for: indexPath) as! AddPlaceSecondTableViewCell
            cell.selectionStyle = .none
            
            cell.placeImageView.image = self.selectedImage
            cell.delegate = self
            cell.tag = indexPath.row
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddPlaceThirdTableViewCell",for: indexPath) as! AddPlaceThirdTableViewCell
            cell.selectionStyle = .none
            
            cell.delegate = self
            cell.tag = indexPath.row
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 227
        }else if indexPath.row == 1 {
            return 180
        }else {
            return 160
        }
    }
    func ImageSelectedClicked()
    {
        let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default , handler:{ (UIAlertAction)in
            print("User click take photo")
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.picker.allowsEditing = false
                self.picker.delegate = self
                self.picker.sourceType = UIImagePickerController.SourceType.camera
                self.picker.cameraCaptureMode = .photo
                self.picker.modalPresentationStyle = .fullScreen
                self.present(self.picker,animated: true,completion: nil)
            }else {
                self.noCamera()
            }
        }))
        alert.addAction(UIAlertAction(title: "Pick from gallery", style: .default , handler:{ (UIAlertAction)in
            print("User click take video")
            print("Choose photo from gallery")
            self.picker.allowsEditing = false
            self.picker.sourceType = .photoLibrary
            self.picker.delegate = self
            self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.picker.modalPresentationStyle = .popover
            self.present(self.picker,animated: true,completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    //MARK: - Delegates image picker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage]
            as? UIImage else {
                return
        }
        var  chosenImage = UIImage()
          chosenImage = image

        self.selectedImage = chosenImage
        self.tableView.reloadData()
        if chosenImage.size.width > 414
        {
            let factor = chosenImage.size.width / 414
            chosenImage =  chosenImage.scaleImageToSize(newSize: CGSize(width: 414, height: chosenImage.size.height/factor))
        }else if chosenImage.size.height > 812
        {
            let factor = chosenImage.size.height / 812
            chosenImage =  chosenImage.scaleImageToSize(newSize: CGSize(width: chosenImage.size.width/factor, height: 812))
        }
        self.uploadImageToAWS(img: chosenImage)
        dismiss(animated:true, completion: nil) //5
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.picker.dismiss(animated: true) {() -> Void in }
    }
    //MARK: - aws methods
    func uploadImageToAWS(img:UIImage)
    {
        ModalClass.startLoading(self.view)
        // getting local path
        var imageURL = NSURL()
        let imageName:String! = "goarunachal";
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! as String
        let localPath = (documentDirectory as NSString).appendingPathComponent(imageName!)

        let data = img.jpegData(compressionQuality: 0.1)
        do
        {
            if data != nil {
                try data?.write(to: URL(fileURLWithPath: localPath), options: .atomic)
            }else{
                return;
            }
        }catch{
            print(error)
        }
        imageURL = NSURL(fileURLWithPath: localPath)
        let S3BucketName: String = Constant.bucketName;
        var _: AWSS3TransferUtilityUploadCompletionHandlerBlock?
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.uploadProgress = {(bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) -> Void in
            DispatchQueue.main.async(execute: {() -> Void in
                //Update progress
            })
        }
        let key : String = NSString(format: "profile_image_%@.png",(String(round(NSDate().timeIntervalSince1970) * 1000).replacingOccurrences(of: ".", with: ""))) as String
        print(key)
        uploadRequest?.body = imageURL as URL
        uploadRequest?.key = key
        uploadRequest?.bucket = S3BucketName
        // uploadRequest?.contentType = "image/" + ".png"
        uploadRequest?.contentType = "image/" + ".png"
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith { (task) -> AnyObject! in
            if let error = task.error {
                DispatchQueue.main.async {
                    ModalClass.stopLoading()
                }
                print("Upload failed ❌ (\(error))");
                let banner = Banner(title: "Upload failed. Please try again.", subtitle: "", image: UIImage(named: ""), backgroundColor: UIColor.red)
                banner.dismissesOnTap = true
                banner.show(duration: 1.5)
            }else{
                DispatchQueue.main.async {
                    ModalClass.stopLoading()
                }
                if task.result != nil{
                    self.s3URL = "https://s3.ap-south-1.amazonaws.com/\(S3BucketName)/\(key)";
                    // self.callWebServicesAddPost("");
                    print("Uploaded to:\n\(self.s3URL!)")
                    DispatchQueue.main.async {
                    }
                }else{
                    print("Unexpected empty result.")
                }
            }
            return nil
        }
    }
    func SubmitClicked()
    {
        self.view.endEditing(true)
        if addPLaceName?.isEmpty == true {
            ModalController.showNegativeCustomAlertWith(title: "", msg: "Please enter your place.")
        }else{
            self.AddPlaceAPI()
        }
    }
    func entryPlaceName(entryNameStr: String, _ sender: Any)
    {
            addPLaceName = entryNameStr
            print("placeName",entryNameStr)
    }
    func AddPlaceAPI(){
        
        ModalClass.startLoading(self.view)
        let str = "\(Constant.BASE_URL)add_place.php"
        var parasm : String  = ""
        if let url = self.s3URL
        {
            parasm  = "name=\(self.addPLaceName!)&imageurl=\(url)"

        }else{
            parasm  = "name=\(self.addPLaceName!)&imageurl="
        }
        
        print("request -",parasm)
        
        WebModel.makePostRequest(str, withParameters: parasm) { (response, success) in
            ModalClass.stopLoading()
            //            }
            if success == true {
                print (response ?? "N/A")
                
                self.FinalApiResponseDict = (response as? NSDictionary)!
                
                if let result =  (self.FinalApiResponseDict.object(forKey: "status") as? String)
                {
                    self.apiStatusStr = result
                }
                if let result =  (self.FinalApiResponseDict.object(forKey: "status") as? NSNumber)        {
                    self.apiStatusStr = String(describing: result)
                }
                
                let x = self.apiStatusStr
                if !(x == "1")
                {
                    ModalController.showNegativeCustomAlertWith(title: self.FinalApiResponseDict.object(forKey: "message") as! String, msg: "")
                }
                else{

                    ModalController.showAlertWith(message: "", title: self.FinalApiResponseDict.object(forKey: "message") as! String)
                    
                    self.navigationController?.popViewController(animated: true)
                    self.tableView.reloadData()
                }
            }else{
                if response == nil
                {
                    print ("connection error")
                    
                    ModalController.showNegativeCustomAlertWith(title: "connection error", msg: "")
                    
                }else{
                    print ("data not in proper json")
                }
            }
        }
    }
}
extension UIImage {
    
    func scaleImageToSize(newSize: CGSize) -> UIImage {
        var scaledImageRect = CGRect.zero
        
        let aspectWidth = newSize.width/size.width
        let aspectheight = newSize.height/size.height
        
        let aspectRatio = max(aspectWidth, aspectheight)
        
        scaledImageRect.size.width = size.width * aspectRatio;
        scaledImageRect.size.height = size.height * aspectRatio;
        scaledImageRect.origin.x = (newSize.width - scaledImageRect.size.width) / 2.0;
        scaledImageRect.origin.y = (newSize.height - scaledImageRect.size.height) / 2.0;
        
        UIGraphicsBeginImageContext(newSize)
        draw(in: scaledImageRect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage!
    }
    //   func imageTapped1(_ sender: UITapGestureRecognizer) {
    //        print("image tapped")
    //
    ////        let vc = LightBoxGallertViewController()
    ////  //      vc.imageURL = self.modal.message
    ////self.navigationController?.pushViewController(vc, animated: true);
    //
    //    }
}
