//
//  AddPlaceThirdTableViewCell.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/26/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
protocol AddPlaceThirdTableViewCellDelegate: class {
    func SubmitClicked()
    
}
class AddPlaceThirdTableViewCell: UITableViewCell {
    @IBOutlet weak var submitBtn: UIButton!
    @IBAction func SubmitBtnClicked(_ sender: Any) {
        self.delegate?.SubmitClicked()
        
    }
    weak var delegate: AddPlaceThirdTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.submitBtn.layer.borderWidth = 2.0
//        self.submitBtn.layer.borderColor = (UIColor(red: 226 / 255.0, green: 226 / 255.0, blue: 226 / 255.0, alpha: 1.0)).cgColor
        self.submitBtn.layer.cornerRadius = self.submitBtn.bounds.size.height/2
        self.submitBtn.clipsToBounds = true

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
