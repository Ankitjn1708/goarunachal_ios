//
//  LoginViewController.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/14/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var loginBottomLabel: UILabel!
    @IBOutlet weak var backbtn: UIButton!
    
    @IBAction func backbtnClicked(_ sender: Any) {
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var signupBtn: UIButton!
    @IBAction func signupBtnClicked(_ sender: Any) {
        let newViewController = SignUPViewController()
       
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    @IBOutlet weak var signupLowerLabel: UILabel!
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBAction func loginBtnClicked(_ sender: Any) {
    }
    @IBAction func loginBtn(_ sender: Any) {
        view.endEditing(true)
        if (self.phoneTxtFld.text?.isEmpty)! || (self.passwordTxtFld.text?.isEmpty)!{
            let banner = Banner(title: "Please enter all fields.", subtitle: "", image: UIImage(named: ""), backgroundColor: UIColor.red)
            banner.dismissesOnTap = true
            banner.show(duration: 1.5)
            return
        }
//        else if !ModalController.isValidEmail(testStr: loginEmail!){
//            let banner = Banner(title: "Please enter valid email id.", subtitle: "", image: UIImage(named: ""), backgroundColor: UIColor.red)
//            banner.dismissesOnTap = true
//            banner.show(duration: 1.5)
//            return
//        }
        else{
            self.loginAPI()
        }
    }
    @IBOutlet weak var passwordTxtFld: UITextField!
    
    @IBOutlet weak var phoneTxtFld: UITextField!
    @IBAction func forgotPasswordClicked(_ sender: Any) {
        let newViewController = ForgetPasswordViewController()
        
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    @IBOutlet weak var SubmitBtn: UIButton!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true

        // Do any additional setup after loading the view.
       
      
        let paddingViw = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 38))
        self.phoneTxtFld.leftView = paddingViw
        self.phoneTxtFld.leftViewMode = .always
        
        ModalController.roundView(viw : self.phoneTxtFld)
        ModalController.roundView(viw : self.passwordTxtFld)

        
        let paddingViwe = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 38))
        self.passwordTxtFld.leftView = paddingViwe
        self.passwordTxtFld.leftViewMode = .always
        self.passwordTxtFld.isSecureTextEntry = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true

    }
    override func viewDidLayoutSubviews() {
        ModalController.roundView(viw : self.phoneTxtFld)
        ModalController.roundView(viw : self.passwordTxtFld)

    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func loginAPI(){
        
        ModalClass.startLoading(self.view)
        let str = "\(Constant.BASE_URL)login.php"
        
        let parasm : String = "emailmobile=\(self.phoneTxtFld.text!)&password=\(self.passwordTxtFld.text!)"
        print("request -",parasm)
        
        WebModel.makePostRequest(str, withParameters: parasm) { (response, success) in
            ModalClass.stopLoading()
            //            }
            if success == true {
                print (response ?? "N/A")
                
                let ResponseDict : NSDictionary = (response as? NSDictionary)!
                let x = ResponseDict.object(forKey: "status") as! NSNumber
                if !(x == 1)
                {
                    ModalController.showNegativeCustomAlertWith(title: ResponseDict.object(forKey: "message") as! String, msg: "")
                }
                else{
                    ModalController.showSuccessCustomAlertWith(title:"Login successfull.",msg:"")
                    
                    let dict : NSDictionary = NSDictionary(dictionary: (ResponseDict.object(forKey: "UserDetail")  as! NSDictionary)).RemoveNullValueFromDic()
                    
                    ModalController.saveTheContent((ResponseDict.object(forKey: "UserDetail") as? NSDictionary)?.object(forKey: "id") as! NSString, WithKey: "userID")
                    
                    ModalController.saveTheContent(dict as? AnyObject, WithKey:Constant.SavedUserData )
                     NotificationCenter.default.post(name: Notification.Name("Login"), object: nil)
                    self.dismiss(animated: true, completion: nil)

                }
            }else{
                if response == nil
                {
                    print ("connection error")
                    
                    ModalController.showNegativeCustomAlertWith(title: "connection error", msg: "")
                    
                }else{
                    print ("data not in proper json")
                }
            }
        }
    }
}
