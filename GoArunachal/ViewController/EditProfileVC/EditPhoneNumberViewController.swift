//
//  EditPhoneNumberViewController.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 11/16/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
protocol updatePhoneNumberDelegate: class {
    func editPhoneNumberSuccessfull()
}
class EditPhoneNumberViewController: UIViewController {
    @IBOutlet weak var confirmPassword: UITextField!
    var apiStatus: String? = ""
    var responseFinalDict : NSDictionary = NSDictionary()
    weak var delegate: updatePhoneNumberDelegate?

    @IBOutlet weak var currentPassword: UITextField!
    @IBOutlet weak var newpassword: UITextField!
    @IBAction func updateBtnClicked(_ sender: Any) {
        let userData = ModalController.getTheContentForKey(Constant.SavedUserData)
        if (self.currentPassword.text?.isEmpty)! && (self.newpassword.text?.isEmpty)! && (self.confirmPassword.text?.isEmpty)!{
            ModalController.showNegativeCustomAlertWith(title: "", msg: "Please enter something for update.")
        }else if (self.newpassword.text?.count)! < 4 && (self.confirmPassword.text?.count)! < 4 {
            ModalController.showNegativeCustomAlertWith(title: "", msg: "password should have minimum 4 characters.")
            
        }else if self.newpassword.text != self.confirmPassword.text {
            ModalController.showNegativeCustomAlertWith(title: "", msg: "Password and confirm password are not match.")
        }else if self.newpassword.text == userData?.object(forKey: "password") as? String {
            ModalController.showNegativeCustomAlertWith(title: "", msg: "Nothing to update.")
        }
        else{
            self.UpdateProfileAPI()
        }

    }
    
    @IBOutlet weak var updateBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
self .makeNavigationButton()
        // Do any additional setup after loading the view.
       
        let userData = ModalController.getTheContentForKey(Constant.SavedUserData)
        self.currentPassword.text = userData?.object(forKey: "password") as? String
        self.currentPassword.isUserInteractionEnabled = false
        
        self.updateBtn.layer.cornerRadius = self.updateBtn.bounds.size.height/2
        self.updateBtn.clipsToBounds = true
        
        self.currentPassword.isSecureTextEntry = true
        self.newpassword.isSecureTextEntry = true
        self.confirmPassword.isSecureTextEntry = true

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func makeNavigationButton()
    {
        navigationController?.navigationBar.barTintColor = Constant.APPCOLOUR
        
        self.navigationItem.title = "Update Password"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let btn = UIButton(type: .custom)
        
        btn.widthAnchor.constraint(equalToConstant: 12).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        btn.setImage(UIImage(named: "back_icon.png"), for: .normal)
        
        btn.addTarget(self, action: #selector(backBtnClicked(btn:)), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btn)
        self.navigationItem.leftBarButtonItem = item
    }
    @objc func backBtnClicked(btn : UIButton)
    {
        _ = navigationController?.popViewController(animated: true)
        
    }
    func UpdateProfileAPI()
    {
        ModalClass.startLoading(self.view)
        let str = "\(Constant.BASE_URL)changepassword.php"
        
           let parasm = "userid=\((ModalController.getTheContentForKey("userID") as AnyObject))&oldpassword=\(self.currentPassword.text!)&newpass=\(self.newpassword.text!)&confirmnewpass=\(self.confirmPassword.text!)"
            print("request -",parasm)
        
        WebModel.makePostRequest(str, withParameters: parasm) { (response, success) in
            ModalClass.stopLoading()
            
            if success == true {
                // api hit success
                print (response ?? "N/A")
                let ResponseDict : NSDictionary = (response as? NSDictionary)!
                print("responseDict %@",ResponseDict)
                
                //                self.apiStatus = ""
                if let result =  (ResponseDict.object(forKey: "status") as? String)
                {
                    self.apiStatus = result
                }
                if let result =  (ResponseDict.object(forKey: "status") as? NSNumber)        {
                    self.apiStatus = String(describing: result)
                }
                
                let x = self.apiStatus
                if !(x == "1"){
                    
                }else{
                    self.responseFinalDict = (ResponseDict.object(forKey: "UserDetail") as! NSDictionary)
                    print("userDeta response \(self.responseFinalDict)")
                    ModalController.showAlertWith(message: "Password updated.", title: "")
                    let dict : NSDictionary = NSDictionary(dictionary: self.responseFinalDict).RemoveNullValueFromDic()
                    
                    ModalController.saveTheContent(dict as? AnyObject, WithKey:Constant.SavedUserData)
                    
                    self.delegate?.editPhoneNumberSuccessfull()
                    _ = self.navigationController?.popViewController(animated: true)
                }
            }
            else
            {
                if response == nil
                {
                    print ("connection error")
                    ModalController.showNegativeCustomAlertWith(title: "connection error", msg: "")
                }else{
                    print ("data not in proper json")
                }
            }
        }
    }
    
}
