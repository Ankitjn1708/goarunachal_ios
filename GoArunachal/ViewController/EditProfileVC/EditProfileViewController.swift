//
//  EditProfileViewController.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 11/16/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
protocol updateProfileDelegate: class {
    func editSuccessfull()
}
class EditProfileViewController: UIViewController,UITextFieldDelegate {
    var selectedScreen: Int?
    var PhoneNumber: String? = ""
    // reset name 1    edit phone 2   edit password 3
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var lowerLabel: UILabel!
    @IBOutlet weak var updateButton: UIButton!
    weak var delegate: updateProfileDelegate?
    @IBAction func updateBtnClicked(_ sender: Any) {
        view.endEditing(true)
        
        let userData = ModalController.getTheContentForKey(Constant.SavedUserData)
        if selectedScreen == 1 {
            if (self.textField.text?.isEmpty)!{
                ModalController.showNegativeCustomAlertWith(title: "", msg: "Please enter something for update.")
                
            }else if self.textField.text == userData?.object(forKey: "name") as? String {
                ModalController.showNegativeCustomAlertWith(title: "", msg: "Nothing to update.")
            }
            else{
                self.UpdateProfileAPI()
            }
        }else if selectedScreen == 2 {
            if (self.textField.text?.isEmpty)!{
                ModalController.showNegativeCustomAlertWith(title: "", msg: "Please enter something for update.")
            }else if self.textField.text == userData?.object(forKey: "mobile") as? String {
                ModalController.showNegativeCustomAlertWith(title: "", msg: "Nothing to update.")
            }else if (self.textField.text?.count)! < 10  {
                ModalController.showNegativeCustomAlertWith(title: "", msg: "Please enter a valid phone number.")
                
            }else if (self.textField.text?.count)! > 12 {
                ModalController.showNegativeCustomAlertWith(title: "", msg: "Please enter a valid phone number.")
                
            }
            else{
                self.UpdateProfileAPI()
            }
        }
      
    }
    var apiStatus: String? = ""
    var responseFinalDict : NSDictionary = NSDictionary()

    @IBOutlet weak var headerNmaeLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
self .makeNavigationButton()
        // Do any additional setup after loading the view.
        self.updateButton.layer.cornerRadius = self.updateButton.bounds.size.height/2
        self.updateButton.clipsToBounds = true
        
        if selectedScreen == 1 {
            self.headerNmaeLabel.text = "Name"
            textField.attributedPlaceholder = NSAttributedString(string: "Enter name",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 84.0/255.0, green: 84.0/255.0, blue: 84.0/255.0, alpha: 1.0)])
          
            textField.keyboardType = .default
            let userData = ModalController.getTheContentForKey(Constant.SavedUserData)
            textField.text = userData?.object(forKey: "name") as? String
            
        }else if selectedScreen == 2 {
            self.headerNmaeLabel.text = "Mobile No."
            textField.attributedPlaceholder = NSAttributedString(string: "Mobile No.",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 84.0/255.0, green: 84.0/255.0, blue: 84.0/255.0, alpha: 1.0)])
            textField.keyboardType = .numberPad
            let userData = ModalController.getTheContentForKey(Constant.SavedUserData)
            textField.text = userData?.object(forKey: "mobile") as? String
        }else {
            textField.delegate = self
            self.textField.text = "Password"
            
            textField.keyboardType = .default
            let userData = ModalController.getTheContentForKey(Constant.SavedUserData)
            textField.text = userData?.object(forKey: "password") as? String
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func makeNavigationButton()
    {
        navigationController?.navigationBar.barTintColor = Constant.APPCOLOUR
        
        self.navigationItem.title = "Update Profile"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let btn = UIButton(type: .custom)
        
        btn.widthAnchor.constraint(equalToConstant: 12).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        btn.setImage(UIImage(named: "back_icon.png"), for: .normal)
        
        btn.addTarget(self, action: #selector(backBtnClicked(btn:)), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btn)
        self.navigationItem.leftBarButtonItem = item
    }
    @objc func backBtnClicked(btn : UIButton)
    {
        _ = navigationController?.popViewController(animated: true)
       
    }
    func UpdateProfileAPI()
    {
        ModalClass.startLoading(self.view)
        let str = "\(Constant.BASE_URL)updateprofile.php"
        var parasm : String = ""
        if selectedScreen == 1 {
            parasm = "name=\(self.textField.text!)&id=\((ModalController.getTheContentForKey("userID") as AnyObject))"
            print("request -",parasm)
            
        }else if selectedScreen == 2 {
            parasm = "mobile=\(self.textField.text!)&id=\((ModalController.getTheContentForKey("userID") as AnyObject))"
            print("request -",parasm)
        }
        WebModel.makePostRequest(str, withParameters: parasm) { (response, success) in
            ModalClass.stopLoading()
            
            if success == true {
                // api hit success
                print (response ?? "N/A")
                let ResponseDict : NSDictionary = (response as? NSDictionary)!
                print("responseDict %@",ResponseDict)
                
                //                self.apiStatus = ""
                if let result =  (ResponseDict.object(forKey: "status") as? String)
                {
                    self.apiStatus = result
                }
                if let result =  (ResponseDict.object(forKey: "status") as? NSNumber)        {
                    self.apiStatus = String(describing: result)
                }
                
                let x = self.apiStatus
                if !(x == "1"){
                    
                }else{
                    self.responseFinalDict = (ResponseDict.object(forKey: "UserDetail") as! NSDictionary)
                    print("userDeta response \(self.responseFinalDict)")
                    ModalController.showAlertWith(message: "Profile updated.", title: "")
                    let dict : NSDictionary = NSDictionary(dictionary: self.responseFinalDict).RemoveNullValueFromDic()
                    
                    ModalController.saveTheContent(dict as? AnyObject, WithKey:Constant.SavedUserData)

                    self.delegate?.editSuccessfull()
                    _ = self.navigationController?.popViewController(animated: true)
                }
            }
            else
            {
                if response == nil
                {
                    print ("connection error")
                    ModalController.showNegativeCustomAlertWith(title: "connection error", msg: "")
                }else{
                    print ("data not in proper json")
                }
            }
        }
    }
}
