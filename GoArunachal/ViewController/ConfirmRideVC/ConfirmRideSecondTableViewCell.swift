//
//  ConfirmRideSecondTableViewCell.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/27/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
protocol ConfirmRideSecondTableViewCellDelegate: class {
    func ConfirmRideClicked()
    
}
class ConfirmRideSecondTableViewCell: UITableViewCell {
    @IBAction func confirmBtnClicked(_ sender: Any) {
        self.delegate?.ConfirmRideClicked()
    }
    weak var delegate: ConfirmRideSecondTableViewCellDelegate?

    @IBOutlet weak var confirmBtnClicked: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
