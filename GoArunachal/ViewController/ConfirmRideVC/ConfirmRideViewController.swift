//
//  ConfirmRideViewController.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/27/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit

class ConfirmRideViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,ConfirmRideSecondTableViewCellDelegate {
    var tripModal : TripModal = TripModal()
    var ConfirmRideModal : TripCateogry = TripCateogry()
    var TotalPrice : String = ""
    var estimatedTime : String = ""
    var confirmApiResponse : String = ""
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
         tableView.register(UINib(nibName: "TripCateogryTableViewCell", bundle: nil), forCellReuseIdentifier: "TripCateogryTableViewCell");
 tableView.register(UINib(nibName: "ConfirmRideFirstTableViewCell", bundle: nil), forCellReuseIdentifier: "ConfirmRideFirstTableViewCell");
         tableView.register(UINib(nibName: "ConfirmRideSecondTableViewCell", bundle: nil), forCellReuseIdentifier: "ConfirmRideSecondTableViewCell");
        // Do any additional setup after loading the view.
        self .makeNavigationButton()
        self.tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = 82;
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func makeNavigationButton()
    {
        navigationController?.navigationBar.barTintColor = Constant.APPCOLOUR
        
        self.navigationItem.title = "Confirm Ride"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let btn = UIButton(type: .custom)
        
        btn.widthAnchor.constraint(equalToConstant: 12).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        btn.setImage(UIImage(named: "back_icon.png"), for: .normal)
        
        btn.addTarget(self, action: #selector(backBtnClicked(btn:)), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btn)
        self.navigationItem.leftBarButtonItem = item
    }
    @objc func backBtnClicked(btn : UIButton)
    {
        _ = navigationController?.popViewController(animated: true)

    }
    
    //MARK:- UItableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TripCateogryTableViewCell",for: indexPath) as! TripCateogryTableViewCell
            cell.selectionStyle = .none
            cell.toLabel.text = self.tripModal.sourceLocation.placeName;
            cell.fromLabel.text = self.tripModal.destinationLocation.placeName;
            cell.priceLabel.text = self.TotalPrice

            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmRideFirstTableViewCell",for: indexPath) as! ConfirmRideFirstTableViewCell
            cell.selectionStyle = .none
            if ConfirmRideModal.tripCateogryPackageName == "Economy" {
                cell.rideTypeImageVIew.image = UIImage(named: "economy_car_img.png")
            }else if ConfirmRideModal.tripCateogryPackageName == "Standard"{
                cell.rideTypeImageVIew.image = UIImage(named: "standard_car_img.png")
            }else if ConfirmRideModal.tripCateogryPackageName == "Premium" {
                cell.rideTypeImageVIew.image = UIImage(named: "premium_car_img.png")
            }
            cell.costLabel.text = ConfirmRideModal.tripCateogryTotalCost
            cell.timeLabel.text = self.estimatedTime
            cell.rideTypeName.text = ConfirmRideModal.tripCateogryPackageName
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmRideSecondTableViewCell",for: indexPath) as! ConfirmRideSecondTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UITableView.automaticDimension
        }else if indexPath.row == 1 {
            return 330
            
        }else {
            return 160
            
        }
    }
    func ConfirmRideAPI(){
        ModalClass.startLoading(self.view)
        let str = "\(Constant.BASE_URL)booking.php"
        
        let parasm : String = "toid=\(self.tripModal.sourceLocation.placeID)&fromid=\(self.tripModal.destinationLocation.placeID)&packageid=\(self.ConfirmRideModal.tripCateogryID)&userid=\(ModalController.getTheContentForKey("userID") as AnyObject)&totalcost=\(self.ConfirmRideModal.tripCateogryTotalCost)"
        print("request -",parasm)
        
        WebModel.makePostRequest(str, withParameters: parasm) { (response, success) in
            ModalClass.stopLoading()
            //            }
            if success == true {
                print (response ?? "N/A")
                
                let ResponseDict : NSDictionary = (response as? NSDictionary)!
                
                if let result =  (ResponseDict.object(forKey: "status") as? String)
                {
                    self.confirmApiResponse = result
                }
                if let result =  (ResponseDict.object(forKey: "status") as? NSNumber)        {
                    self.confirmApiResponse = String(describing: result)
                }
                let x = self.confirmApiResponse
                if !(x == "1")
                {
                    ModalController.showNegativeCustomAlertWith(title: "", msg: (ResponseDict.object(forKey: "message") as? String ?? "No cabs are available to take your ride.") )
                }
                else{
                    ModalController.showAlertWith(message: "Your ride booked successfully", title: "info")

                      NotificationCenter.default.post(name: Notification.Name("replanRide"), object: nil)
                    
                    let bookingRide : NSArray = (ResponseDict.object(forKey: "FinalBookingInfo") as! NSArray)
                    
                    var passingDict : NSDictionary = NSDictionary()
                    if bookingRide.count > 0 {
                        
                        passingDict = bookingRide.object(at: 0) as! NSDictionary
                        
                        let bookinginfo = passingDict.RemoveNullValueFromDic()
                        
                          ModalController.saveTheContent(bookinginfo , WithKey:Constant.SavedRideBookingInfo)
                         NotificationCenter.default.post(name: Notification.Name("rideBookingInfo"), object: bookinginfo)
                    }
              let controllerArr = self.navigationController?.viewControllers as! [UIViewController]
                 for view in controllerArr
                 {
                    if view is HomeViewController
                    {
                        self.navigationController?.popToViewController(view, animated: true)
                        break;
                    }
                  }
            }
            }else{
                if response == nil
                {
                    print ("connection error")
                    
                    ModalController.showNegativeCustomAlertWith(title: "connection error", msg: "")
                }else{
                    print ("data not in proper json")
                }
            }
        }
    }
    func ConfirmRideClicked()
    {
    if (ModalController.isUserLoggedIn() == false){
        let alert = UIAlertController(title: "Info", message: "Please login first for booking your ride.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            // logout code
            let vc = LoginViewController(nibName: "LoginViewController", bundle: nil)
            let navLogin = UINavigationController.init(rootViewController: vc)
            navLogin.isNavigationBarHidden = true
            self.navigationController?.present(navLogin, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
            return
        }))
        self.present(alert, animated: true, completion: nil)
        }else{
            self .ConfirmRideAPI()
        }
            }
}
