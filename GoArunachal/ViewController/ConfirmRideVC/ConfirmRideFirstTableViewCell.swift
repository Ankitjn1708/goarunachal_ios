//
//  ConfirmRideFirstTableViewCell.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/27/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit

class ConfirmRideFirstTableViewCell: UITableViewCell {
    @IBOutlet weak var rideTypeName: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var rideTypeImageVIew: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
