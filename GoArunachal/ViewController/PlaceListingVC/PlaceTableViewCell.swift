//
//  PlaceTableViewCell.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/13/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
protocol PlaceTableViewCellDelegate: class {
    func SelectClicked (PlaceNameStr: PlaceTableViewCell)
}
class PlaceTableViewCell: UITableViewCell {
    @IBOutlet weak var placeNameLabel: UILabel!
    weak var delegate: PlaceTableViewCellDelegate?

    @IBAction func selectBtnClicked(_ sender: Any) {
self.delegate?.SelectClicked(PlaceNameStr: self)
    }
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var placeImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
