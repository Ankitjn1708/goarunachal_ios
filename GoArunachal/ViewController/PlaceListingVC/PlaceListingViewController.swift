//
//  PlaceListingViewController.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/13/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
import SDWebImage
protocol PlaceListingViewControllerDelegate: class {
    func SelectPlaceClicked (placeModal: PlaceListing, isFromSource : Bool)
}
class PlaceListingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,PlaceTableViewCellDelegate,UITextFieldDelegate {
    @IBOutlet weak var tableView: UITableView!
    var ResponseDict = NSDictionary()
    var placeListArray = NSArray()
    var inFromSourceLocation:Bool = false
    var isMoreDataAvailable: Bool = false
    var currentPageNumber: Int = 1
    
    var fromID : String = ""
    weak var delegate: PlaceListingViewControllerDelegate?
    var allPlacesArray = [PlaceListing]()
    @IBOutlet weak var searchTextFld: UITextField!
    @IBAction func loginbtn(_ sender: Any) {
        let newViewController = LoginViewController()
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        isMoreDataAvailable = false
        currentPageNumber = 1
        
        self.tableView.separatorStyle = .none
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.makeNavigationButton()
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        tableView.register(UINib(nibName: "PlaceTableViewCell", bundle: nil), forCellReuseIdentifier: "PlaceTableViewCell");
        ModalClass.startLoading(self.view)
        self.placeListingAPI(text: "")

        let paddingViw = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 45))
        self.searchTextFld.leftView = paddingViw
        self.searchTextFld.leftViewMode = .always
        self.searchTextFld.delegate = self as UITextFieldDelegate
        
        self.searchTextFld.layer.borderWidth = 2.0
        self.searchTextFld.layer.borderColor = (UIColor(red: 226 / 255.0, green: 226 / 255.0, blue: 226 / 255.0, alpha: 1.0)).cgColor
        self.searchTextFld.layer.cornerRadius = self.searchTextFld.bounds.size.height/2
        self.searchTextFld.clipsToBounds = true

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func makeNavigationButton()
    {
        navigationController?.navigationBar.barTintColor = Constant.APPCOLOUR
        
        self.navigationItem.title = "Choose Location"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        //backButton
        let btn = UIButton(type: .custom)
        btn.widthAnchor.constraint(equalToConstant: 12).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        btn.setImage(UIImage(named: "back_icon.png"), for: .normal)
        
        btn.addTarget(self, action: #selector(backBtnClicked(btn:)), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btn)
        
        // AddPlace Button
        let addPlacebtn = UIButton(type: .custom)
        addPlacebtn.widthAnchor.constraint(equalToConstant: 25).isActive = true
        addPlacebtn.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        addPlacebtn.setImage(UIImage(named: "add_icon.png"), for: .normal)
        
        addPlacebtn.addTarget(self, action: #selector(addPlaceBtnClicked(addPlacebtn:)), for: .touchUpInside)
        let addBtnItem = UIBarButtonItem(customView: addPlacebtn)
        self.navigationItem.rightBarButtonItem = addBtnItem
        self.navigationItem.leftBarButtonItem = item
    }
    @objc func backBtnClicked(btn : UIButton)
    {
        _ = navigationController?.popViewController(animated: true)
      
    }
    
    @objc func addPlaceBtnClicked(addPlacebtn : UIButton)
    {
        if (ModalController.isUserLoggedIn() == false){
            let alert = UIAlertController(title: "Info", message: "Please login first to add your place.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                // logout code
                let vc = LoginViewController(nibName: "LoginViewController", bundle: nil)
                let navLogin = UINavigationController.init(rootViewController: vc)
                navLogin.isNavigationBarHidden = true
                self.navigationController?.present(navLogin, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
                return
            }))
            self.present(alert, animated: true, completion: nil)
        }else{
            let newViewController = AddPlaceViewController()
        self.navigationController?.pushViewController(newViewController, animated: true)
        }
        
    }
// mark- UITableViewDelegate

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isMoreDataAvailable == true {
             return allPlacesArray.count+1
        }else{
            return allPlacesArray.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row<allPlacesArray.count{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceTableViewCell",for: indexPath) as! PlaceTableViewCell
            cell.selectionStyle = .none
            
            let placeList = allPlacesArray[indexPath.row]
            
            var myImage :String = placeList.placeImage
            
            
            myImage = myImage.replacingOccurrences(of: " "
                , with: "%20")
            cell.placeImageView.sd_setImage(with: URL(string: myImage), placeholderImage: UIImage(named: "placehold_newcamp"))
            
            cell.placeNameLabel.text = placeList.placeName
            cell.delegate = self
            cell.tag = indexPath.row
            return cell
        }else{
          return self.loadingCell()
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.tag == 999999 {
            currentPageNumber += 1
            placeListingAPI(text: self.searchTextFld.text ?? "")
        }
    }
    func loadingCell() -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        //  activityIndicator.center = cell.center;
        cell.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        cell.tag = 999999
        cell.contentView.backgroundColor = UIColor.clear
        cell.backgroundColor = cell.contentView.backgroundColor
        activityIndicator.frame = CGRect(x: (UIScreen.main.bounds.size.width / 2) - 10, y: 12, width: 20, height: 20)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func placeListingAPI(text : String)
    {
        let str = "\(Constant.BASE_URL)locationlist.php?PageNo=\(currentPageNumber)&Search=\(text)&FromId=\(self.fromID)"
        print("apiRequest %@", str)
        WebModel.makeGetRequest(str) { (response, success) in
            ModalClass.stopLoading()
            if success == true {
                // api hit success
                self.ResponseDict  = (response as? NSDictionary)!
                print("ResponseDictionary %@",self.ResponseDict)
                if self.currentPageNumber == 1{
                    self.allPlacesArray.removeAll()
                }
                guard let arr = self.ResponseDict.object(forKey: "LocationList") as? NSArray else{
                    self.isMoreDataAvailable = false
                    self.tableView.reloadData()
                    return
                }
                if arr.count < 10
                {
                    self.isMoreDataAvailable = false
                }else{
                    self.isMoreDataAvailable = true
                }
                
                self.placeListArray = NSArray.init(array: arr)
               
                for placeList in self.placeListArray{
                    let dict = placeList as! NSDictionary
                    let placeListModal = PlaceListing(dict : dict)
                    self.allPlacesArray.append(placeListModal)
                }
                self.tableView.reloadData()
            }else{
                if response == nil
                {
                    print ("connection error")
                    let banner = Banner(title: "connection error", subtitle: "", image: UIImage(named: ""), backgroundColor: UIColor.red)
                    banner.dismissesOnTap = true
                    banner.show(duration: 1.5)
                    return
                }else{
                    print ("data not in proper json")
                }
            }
        }
    }
    func SelectClicked (PlaceNameStr: PlaceTableViewCell)
    {
        let placeModal = allPlacesArray[PlaceNameStr.tag]
        self.delegate?.SelectPlaceClicked(placeModal: placeModal,isFromSource: self.inFromSourceLocation)
        self.navigationController?.popViewController(animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        isMoreDataAvailable = false
        currentPageNumber = 1
        var searchString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
    
        
        searchString = searchString.replacingOccurrences(of: " "
            , with: "%20") as NSString
        //        let searchString = textField.text! + string
       // let searchString = textField.text!.replacingCharacters(in: Range(range, in: textField.text! ) ?? "", with: string)

        print(searchString)
       
        self.placeListingAPI(text: searchString as String)
        
//        if self.tag == 3{
//
//            let MAX_LENGTH = 10
//
//            let newLength: Int = (self.entryFieldTxtFld.text?.count)! + string.count - range.length
//            if newLength > MAX_LENGTH {
//                return false
//            } else {
//                return true
//            }
//
//        }
        return true
    }
    
}
