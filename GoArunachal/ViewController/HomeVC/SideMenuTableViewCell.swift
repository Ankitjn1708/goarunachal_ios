//
//  SideMenuTableViewCell.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/15/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {
    @IBOutlet weak var slidernameLabel: UILabel!
    
    @IBOutlet weak var bottomLineLabel: UILabel!
    @IBOutlet weak var sliderImgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
