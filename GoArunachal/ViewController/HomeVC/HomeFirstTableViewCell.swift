//
//  HomeFirstTableViewCell.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/13/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
protocol HomeFirstTableViewCellDelegate: class {
    func CallDriver(sender : HomeFirstTableViewCell)
    func CancelRide ()
}
class HomeFirstTableViewCell: UITableViewCell {
    weak var delegate: HomeFirstTableViewCellDelegate?

    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var driverStatusLabel: UILabel!
    @IBOutlet weak var profilePicImage: UIImageView!
    
    @IBAction func callButtonClicked(_ sender: Any) {
        self.delegate?.CallDriver(sender: self)
    }
    @IBOutlet weak var mobileNumber: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.delegate?.CancelRide()
        
    }
    @IBOutlet weak var callButton: UIButton!
    
    @IBOutlet weak var vehicleNumber: UILabel!
    @IBOutlet weak var driverStatusImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
