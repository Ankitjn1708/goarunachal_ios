//
//  HomeFourthTableViewCell.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/13/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
protocol HomeFourthTableViewCellDelegate: class {
    func NextClicked()
    
}
class HomeFourthTableViewCell: UITableViewCell {
    @IBOutlet weak var nextBtn: UIButton!
    @IBAction func nextbtnClicked(_ sender: Any) {
        self.delegate?.NextClicked()
    }
    weak var delegate: HomeFourthTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
