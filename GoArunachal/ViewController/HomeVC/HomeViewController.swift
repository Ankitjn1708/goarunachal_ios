//
//  HomeViewController.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/9/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
import SideMenu
import Auk

class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,HomeThirdTableViewCellDelegate,PlaceListingViewControllerDelegate,HomeFourthTableViewCellDelegate,HomeFirstTableViewCellDelegate {
   
    var refreshControl = UIRefreshControl()
    var isFromReplanRide:Bool = false
    var tripModal : TripModal = TripModal()
    @IBOutlet weak var tableView: UITableView!
    var bannerResponseFinalDict : NSDictionary = NSDictionary()
    var rideBookedUserDict : NSDictionary = NSDictionary()

    var BannerApiStatus: String? = ""
    var bannerArray = NSArray()

    var cancelRideApiStatus: String? = ""
    var cancelRideApiResponseDict : NSDictionary = NSDictionary()

    var driverInfoApiStatus: String? = ""
    var driverInfoApiResponseDict : NSDictionary = NSDictionary()
    var driverInfoApiArray = NSArray()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.BannerApiGET()
        self.makeNavigationButton()
        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: "HomeFirstTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeFirstTableViewCell");
        tableView.register(UINib(nibName: "HomeSecondTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeSecondTableViewCell");
        tableView.register(UINib(nibName: "HomeThirdTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeThirdTableViewCell");
          tableView.register(UINib(nibName: "HomeFourthTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeFourthTableViewCell");
        self.tableView.separatorStyle = .none
        
        NotificationCenter.default.addObserver(self, selector: #selector(LogoutClicked), name: Notification.Name("Logout"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LogInClicked), name: Notification.Name("Login"), object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(ReplanRideClicked), name: Notification.Name("replanRide"), object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(UserBookingInfo), name: Notification.Name("rideBookingInfo"), object: nil)
        
        
        let newViewController = SideMenuViewController()

        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: newViewController)
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        
//        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
//        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        // (Optional) Prevent status bar area from turning black when menu appears:
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuPresentMode = .menuSlideIn;
        SideMenuManager.default.menuWidth = 0.75 * UIScreen.main.bounds.size.width

        self.addUIRefreshToTable()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        
    if (ModalController.getTheContentForKey(Constant.SavedRideBookingInfo) == nil || ModalController.getTheContentForKey(Constant.SavedRideBookingInfo) is NSNull) || (ModalController.isUserLoggedIn() == false) {
            
        }else{
        self.driverInfoApiResponseDict = ModalController.getTheContentForKey(Constant.SavedRideBookingInfo) as! NSDictionary
            self.DriverInformationAPI()
        }
        self.tableView.reloadData()

    }
// MARK: - AddUIRefreshControl
func addUIRefreshToTable() {
        refreshControl = UIRefreshControl()
        tableView.addSubview(refreshControl)
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.lightGray
        refreshControl.addTarget(self, action: #selector(refreshTable), for: .valueChanged)
    }
    @objc func refreshTable() {
 if (ModalController.getTheContentForKey(Constant.SavedRideBookingInfo) == nil || ModalController.getTheContentForKey(Constant.SavedRideBookingInfo) is NSNull) || (ModalController.isUserLoggedIn() == false) {
    if self.refreshControl.isRefreshing {
        self.refreshControl.endRefreshing()
    }
    
        }else{
                DriverInformationAPI()

        }
    }
    @objc func LogInClicked() {
        self.tableView.reloadData()
    }
    @objc func ReplanRideClicked() {
        self.tripModal.sourceLocation.placeName = ""
        self.tripModal.destinationLocation.placeName = ""
        
        self.tableView.reloadData()
    }
    @objc func UserBookingInfo(_notification: Notification) {
        
       self.driverInfoApiResponseDict = _notification.object as! NSDictionary
        
        self.tripModal.sourceLocation.placeName = ""
        self.tripModal.destinationLocation.placeName = ""

        self.tableView.reloadData()
    }
  @objc func LogoutClicked() {
    let alert = UIAlertController(title: "Info", message: "Are you sure you want to logout?", preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
        // logout code
        ModalController.removeTheContentForKey(Constant.SavedUserData)
        ModalController.removeTheContentForKey("userID")
        ModalController.removeTheContentForKey(Constant.SavedRideBookingInfo)
        ModalController.showSuccessCustomAlertWith(title: "Logout successfully", msg: "")
        self.tripModal.sourceLocation.placeName = ""
        self.tripModal.destinationLocation.placeName = ""
        
        self.tableView.reloadData()

    }))
    alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
        return
    }))
    
    self.present(alert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    // mark- tableViewDelegate
    
    func makeNavigationButton()
    {
        navigationController?.navigationBar.barTintColor = Constant.APPCOLOUR
    
        self.navigationItem.title = "GoArunachal"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

        let btn = UIButton(type: .custom)
       
        btn.widthAnchor.constraint(equalToConstant: 32).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 22).isActive = true

        btn.setImage(UIImage(named: "nav_btn"), for: .normal)
   
        btn.addTarget(self, action: #selector(menuClicked(btn:)), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btn)
        self.navigationItem.leftBarButtonItem = item
        
    }
    @objc func menuClicked(btn : UIButton)
    {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
        return
    }
 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeFirstTableViewCell",for: indexPath) as! HomeFirstTableViewCell
            cell.selectionStyle = .none
            if (ModalController.getTheContentForKey(Constant.SavedRideBookingInfo) == nil || ModalController.getTheContentForKey(Constant.SavedRideBookingInfo) is NSNull) || (ModalController.isUserLoggedIn() == false) {
                cell.contentView.isHidden = true;
            }else{
                cell.contentView.isHidden = false;
            }
        
            if self.driverInfoApiResponseDict.count > 0{
            
                let driverName:String = (self.driverInfoApiResponseDict.object(forKey: "DriverDetail") as! NSDictionary).object(forKey: "name") as! String
                
            cell.driverStatusLabel.text = driverName + " will meet you to take your ride."
                
            cell.profileName.text = (self.driverInfoApiResponseDict.object(forKey: "DriverDetail") as! NSDictionary).object(forKey: "name") as? String
                
            cell.mobileNumber.text = (self.driverInfoApiResponseDict.object(forKey: "DriverDetail") as! NSDictionary).object(forKey: "mobile") as? String

            let carName:String = (self.driverInfoApiResponseDict.object(forKey: "DriverDetail") as! NSDictionary).object(forKey: "carname") as! String
                
            let carNumber:String = (self.driverInfoApiResponseDict.object(forKey: "DriverDetail") as! NSDictionary).object(forKey: "carno") as! String
                
            cell.vehicleNumber.text = carName + "\n " + carNumber
                
            cell.profilePicImage.layer.cornerRadius = cell.profilePicImage.layer.frame.size.height / 2
            cell.profilePicImage.clipsToBounds = true
                cell.profilePicImage.layer.borderColor = UIColor.clear.cgColor
                cell.profilePicImage.layer.borderWidth = 1.0
                
            var myPic  = (self.driverInfoApiResponseDict.object(forKey: "DriverDetail") as! NSDictionary).object(forKey: "image") as! String
                
            if !(myPic is NSNull)
            {
                    myPic = (myPic.replacingOccurrences(of: " "
                        , with: "%20") as AnyObject) as! String
                    cell.profilePicImage.sd_setImage(with: URL(string: myPic ), placeholderImage: UIImage(named: "boy.png"))
                }
                else{
                    cell.profilePicImage.image = UIImage(named: "boy.png")
                }
            
            }
            cell.delegate = self
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeSecondTableViewCell",for: indexPath) as! HomeSecondTableViewCell
            cell.selectionStyle = .none
            
            if self.bannerArray.count > 0 {
            for index in 0..<self.bannerArray.count {

            cell.scrollView.auk.show(url: (self.bannerArray[indexPath.row] as AnyObject).value(forKey: "image") as! String )

            }
        }
            // Scroll images automatically with the interval of 3 seconds
            cell.scrollView.auk.startAutoScroll(delaySeconds: 2)
            
//            // Stop auto-scrolling of the images
//            cell.scrollView.auk.stopAutoScroll()

            // Show local image
//            if let image = UIImage(named: "bird.jpg") {
//                cell.scrollView.auk.show(image: image)
//            }
//
            // Return the number of pages in the scroll view
//            cell.scrollView.auk.numberOfPages
//
//            // Get the index of the current page or nil if there are no pages
//            cell.scrollView.auk.currentPageIndex
//
//            // Return currently displayed images
//            cell.scrollView.auk.images
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeThirdTableViewCell",for: indexPath) as! HomeThirdTableViewCell
            cell.selectionStyle = .none
            cell.pickupTextField.text = self.tripModal.sourceLocation.placeName;
            cell.dropTextField.text = self.tripModal.destinationLocation.placeName
            
            if !(self.tripModal.sourceLocation.placeName == "" ) && !(self.tripModal.destinationLocation.placeName == "") {
                cell.middleLabel.isHidden = false
            }else{
                cell.middleLabel.isHidden = true
            }
            cell.delegate = self
            cell.tag = indexPath.row
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeFourthTableViewCell",for: indexPath) as! HomeFourthTableViewCell
            cell.selectionStyle = .none
            
            cell.delegate = self
            return cell
        }
    }
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
              if (ModalController.getTheContentForKey(Constant.SavedRideBookingInfo) == nil || ModalController.getTheContentForKey(Constant.SavedRideBookingInfo) is NSNull) || (ModalController.isUserLoggedIn() == false) {
                return 0;
              }else{
                return 110;
            }
        }else if indexPath.row == 1 {
            return (UIScreen.main.bounds.width)/2
            
        }else if indexPath.row == 2 {
            return 280
        }else{
            return 80
        }
    }
   // mark- delegateMethod
    func PickupClicked() {
        let newViewController = PlaceListingViewController()
        newViewController.delegate = self
        newViewController.inFromSourceLocation = true
        newViewController.fromID = self.tripModal.destinationLocation.placeID
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
     func DropClicked() {
        let newViewController = PlaceListingViewController()
        newViewController.delegate = self
        newViewController.inFromSourceLocation = false
        newViewController.fromID = self.tripModal.sourceLocation.placeID
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    func CallDriver(sender : HomeFirstTableViewCell)
    {
        let cell = sender
        
        if let url = URL(string: "tel://\(cell.mobileNumber.text!))"),
            
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            // add error message here
        }
    }
    func CancelRide (){
        self.CancelRideAPI()
    }
    func SelectPlaceClicked (placeModal: PlaceListing, isFromSource : Bool)
 {
    if  isFromSource == true {
        self.tripModal.sourceLocation = placeModal;
        print("source", self.tripModal.sourceLocation)

    }else{
        self.tripModal.destinationLocation = placeModal;
        print("destination", self.tripModal.destinationLocation)
    }
    self.tableView .reloadData()
    }
    func NextClicked()
    {
        if self.tripModal.sourceLocation.placeName == "" {
            ModalController.showNegativeCustomAlertWith(title: "", msg: "please select pick-up location.")
        }else if self.tripModal.destinationLocation.placeName == "" {
             ModalController.showNegativeCustomAlertWith(title: "", msg: "please select drop location.")
        }else if self.tripModal.sourceLocation.placeID == self.tripModal.destinationLocation.placeID {
             ModalController.showNegativeCustomAlertWith(title: "", msg: "Your source and destination location are same.")
        }
        else{
            let newViewController = TripCateogryViewController()
        newViewController.tripModal = self.tripModal;
        self.navigationController?.pushViewController(newViewController, animated: true)
       }
    }

func BannerApiGET()
    {
//        ModalClass.startLoading(self.view)
        let str = "\(Constant.BASE_URL)banner.php"
        
        WebModel.makeGetRequest(str) { (response, success) in
            ModalClass.stopLoading()
            if success == true {
                // api hit success
                
                print (response ?? "N/A")
                self.bannerResponseFinalDict = (response as? NSDictionary)!
                print("bannerResponseFinalDict %@",self.bannerResponseFinalDict)
                
                self.BannerApiStatus = ""
                if let result =  (self.bannerResponseFinalDict.object(forKey: "Status") as? String)
                {
                    self.BannerApiStatus = result
                }
                if let result =  (self.bannerResponseFinalDict.object(forKey: "Status") as? NSNumber)        {
                    self.BannerApiStatus = String(describing: result)
                }
                
                let x = self.BannerApiStatus
                if !(x == "1"){
                    
                }else{
                    self.bannerArray = (self.bannerResponseFinalDict.object(forKey: "BannerList") as! NSArray)
                    print("Banner response \(self.bannerArray)")
                    self.tableView.reloadData()
                }
            }
            else
            {
                if response == nil
                {
                    print ("connection error")
                    ModalController.showNegativeCustomAlertWith(title: "connection error", msg: "")
                }else{
                    print ("data not in proper json")
                }
            }
        }
    }
    func CancelRideAPI()
    {
        let rideBookingInfo: NSDictionary = ModalController.getTheContentForKey(Constant.SavedRideBookingInfo) as! NSDictionary
        
        let bookingID = (rideBookingInfo.object(forKey: "BookingInfo") as! NSDictionary).object(forKey: "id") as! String
        
        
        ModalClass.startLoading(self.view)
        let str = "\(Constant.BASE_URL)usercancelride.php"
        
        let  parasm = "bookingid=\(bookingID)&userid=\((ModalController.getTheContentForKey("userID") as AnyObject))&cancelby=\("customer")"
        print("request -",parasm)
        
        WebModel.makePostRequest(str, withParameters: parasm) { (response, success) in
            ModalClass.stopLoading()
            
            if success == true {
                // api hit success
                let ResponseDict : NSDictionary = (response as? NSDictionary)!
                print("cancelRideApiResponseDict %@",ResponseDict)
                
                if let result =  (ResponseDict.object(forKey: "status") as? String)
                {
                    self.cancelRideApiStatus = result
                }
                if let result =  (ResponseDict.object(forKey: "status") as? NSNumber)        {
                    self.cancelRideApiStatus = String(describing: result)
                }
                
                let x = self.cancelRideApiStatus
                if !(x == "1"){
                    ModalController.showNegativeCustomAlertWith(title: "", msg: "something went wrong.")
                }else{
                    ModalController.showAlertWith(message: "you are successfully cancel your ride.", title: "")
                    [ModalController .removeTheContentForKey(Constant.SavedRideBookingInfo)]
                }
                self.tableView.reloadData()

            }else{
                if response == nil{
                    print ("connection error")
                    ModalController.showNegativeCustomAlertWith(title: "connection error", msg: "")
                }else{
                    print ("data not in proper json")
                }
            }
        }
    }
    func DriverInformationAPI()
    {
//          ModalClass.startLoading(self.view)

        let rideBookingInfo: NSDictionary = ModalController.getTheContentForKey(Constant.SavedRideBookingInfo) as! NSDictionary
        
        guard let driverID = (rideBookingInfo.object(forKey: "BookingInfo") as? NSDictionary)?.object(forKey: "driverid") as? String
        else{
            return;
        }
        let str = "\(Constant.BASE_URL)driversidebooking.php"
        let  parasm = "driverid=\(driverID)"
        print("request -",parasm)
        WebModel.makePostRequest(str, withParameters: parasm) { (response, success) in
            ModalClass.stopLoading()
        if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            if success == true {
                // api hit success
//                print (response ?? "N/A")
                let ResponseDict : NSDictionary = (response as? NSDictionary)!
               print("ResponseDict %@",ResponseDict)
                
                if let result =  (ResponseDict.object(forKey: "status") as? String)
                {
                    self.driverInfoApiStatus = result
                }
                if let result =  (ResponseDict.object(forKey: "status") as? NSNumber)        {
                    self.driverInfoApiStatus = String(describing: result)
                }
                let x = self.driverInfoApiStatus
                if !(x == "1"){
                    ModalController.showNegativeCustomAlertWith(title: "", msg: (ResponseDict.object(forKey: "BookingHistory") as? String ?? "No Current ride available."))
                ModalController.removeTheContentForKey(Constant.SavedRideBookingInfo)
                    self.tableView .reloadData()
                    print("SavedRideBookingInfo \(Constant.SavedRideBookingInfo)")
                    
                }else{
    
                let bookingRideInfo : NSArray = (ResponseDict.object(forKey: "FinalBookingInfo") as! NSArray)

                    var passingDict : NSDictionary = NSDictionary()
                    if bookingRideInfo.count > 0 {
                        
                        passingDict = bookingRideInfo.object(at: 0) as! NSDictionary
                        self.driverInfoApiResponseDict = passingDict.RemoveNullValueFromDic()
                        ModalController.saveTheContent(self.driverInfoApiResponseDict , WithKey:Constant.SavedRideBookingInfo)
                    }
                    print("driverInfoApiResponseDict \(self.driverInfoApiResponseDict)")

                    self.tableView.reloadData()
                }
            }else{
                if response == nil{
                    print ("connection error")
                    ModalController.showNegativeCustomAlertWith(title: "connection error", msg: "")
                }else{
                    print ("data not in proper json")
                }
            }
        }
    }
}
