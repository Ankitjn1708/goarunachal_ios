//
//  SideMenuViewController.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/14/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
import SideMenu
class SideMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var imageArray = NSArray()
    var tableData = NSArray()
    var loginTableData = NSArray()
    var headeraName: String? = ""

    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var loginUserEmail: UILabel!
    @IBOutlet weak var loginUserName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        tableView.register(UINib(nibName: "SideMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "SideMenuTableViewCell");
        
        self.tableView.separatorStyle = .none
        imageArray = ["home2.png","myProfile.png","Add_photo.png", "login.png",  "about-app.png", "t&condition.png", "phone-book3.png"]
        
        tableData = ["Home","My Profile", "Add Your Location", "Login/Signup", "About App", "Terms & Conditions", "Contact Us"]
        
        loginTableData = ["Home", "My Profile", "Add Your Location", "Logout", "About App", "Terms & Conditions", "Contact Us"]

    }
    /*
    // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override func viewWillAppear(_ animated: Bool) {
        if !(ModalController.isUserLoggedIn() == false){
            let userData = ModalController.getTheContentForKey(Constant.SavedUserData)
            self.loginUserName.text = userData?.object(forKey: "name") as? String
            self.loginUserEmail.text = userData?.object(forKey: "mobile") as? String
            self.profileImg.isHidden = false
            self.profileImg.layer.cornerRadius = self.profileImg.layer.frame.size.width / 2
            self.profileImg.clipsToBounds = true
            self.profileImg.layer.borderColor = UIColor.clear.cgColor
            self.profileImg.layer.borderWidth = 1.0
            var myPic  = userData?.object(forKey: "imgurl") as? AnyObject
            
            if !(myPic is NSNull)
            {
                myPic = myPic?.replacingOccurrences(of: " "
                    , with: "%20") as AnyObject
                self.profileImg.sd_setImage(with: URL(string: myPic as! String), placeholderImage: UIImage(named: "boy.png"))
            }
            else{
                self.profileImg.image = UIImage(named: "boy.png")
            }
            }else{
            self.loginUserName.text = ""
            self.loginUserEmail.text = ""
            self.profileImg.isHidden = true
        }
        self.tableView.reloadData()
    }
    // mark- tableViewDelegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell",for: indexPath) as! SideMenuTableViewCell
        cell.selectionStyle = .none
 if (ModalController.isUserLoggedIn() == false){
    cell.slidernameLabel.text = self.tableData.object(at: indexPath.row) as? String

        }else{
    cell.slidernameLabel.text = self.loginTableData.object(at: indexPath.row) as? String

        }
        cell.sliderImgView.image = UIImage(named: (self.imageArray.object(at: indexPath.row) as! String))
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        OperationQueue.main.addOperation {
           // self.dismiss(animated: true, completion: nil)
        }

        if indexPath.row == 0 {
            self.dismiss(animated: true, completion: nil)
            
        }else if indexPath.row == 1 {
            if (ModalController.isUserLoggedIn() == false){
                ModalController.showAlertWith(message: "Please login first.", title: "info")
            }else{
                let newViewController = MyProfileViewController()
                newViewController.headeraName = "MyProfile"
                self.navigationController?.pushViewController(newViewController, animated: true)
            }
        } else if indexPath.row == 2 {
            if (ModalController.isUserLoggedIn() == false){
                let alert = UIAlertController(title: "Info", message: "Please login first to add your place.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                    // logout code
                    let vc = LoginViewController(nibName: "LoginViewController", bundle: nil)
                    let navLogin = UINavigationController.init(rootViewController: vc)
                    navLogin.isNavigationBarHidden = true
                    self.navigationController?.present(navLogin, animated: true, completion: nil)
                }))
                alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
                    return
                }))
                self.present(alert, animated: true, completion: nil)
            }else{
                let newViewController = AddPlaceViewController()
                self.navigationController?.pushViewController(newViewController, animated: true)
            }
        }
        else if indexPath.row == 3{
            if (ModalController.isUserLoggedIn() == false){
                let vc = LoginViewController(nibName: "LoginViewController", bundle: nil)
               let navLogin = UINavigationController.init(rootViewController: vc)
                navLogin.isNavigationBarHidden = true
                self.navigationController?.present(navLogin, animated: true, completion: nil)
                
            }else{
                OperationQueue.main.addOperation {
                    self.dismiss(animated: true, completion: nil)
                    NotificationCenter.default.post(name: Notification.Name("Logout"), object: nil)
                    
                }
            }
        }else if indexPath.row == 4{
            let newViewController = AboutAppViewController()
            self.navigationController?.pushViewController(newViewController, animated: true)
            
        }else if indexPath.row == 5{
            let newViewController = TermsAndConditionsViewController()
            self.navigationController?.pushViewController(newViewController, animated: true)
            
        }else{
            let newViewController = SupportViewController()
            self.navigationController?.pushViewController(newViewController, animated: true)
        }
    }
    

}
