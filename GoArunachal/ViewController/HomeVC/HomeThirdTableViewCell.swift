//
//  HomeThirdTableViewCell.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/13/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
protocol HomeThirdTableViewCellDelegate: class {
    func PickupClicked()
    func DropClicked()

}

class HomeThirdTableViewCell: UITableViewCell {
    @IBOutlet weak var dropTextField: UITextField!
    weak var delegate: HomeThirdTableViewCellDelegate?

    @IBOutlet weak var middleLabel: UILabel!
    @IBAction func dropClicked(_ sender: Any) {
        self.delegate?.DropClicked()
    }
    @IBAction func pickupClicked(_ sender: Any) {
        self.delegate?.PickupClicked()
    }
    @IBOutlet weak var pickupTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
