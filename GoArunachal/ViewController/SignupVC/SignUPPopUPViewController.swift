//
//  SignUPPopUPViewController.swift
//  Scene Ahead
//
//  Created by Sanganan on 21/08/18.
//  Copyright © 2018 aishwarya sanganan. All rights reserved.
//

import UIKit
protocol SignUPPopUPViewControllerDelegate: class {
    func backgroundClickedOnPopUP(isEmail:Bool)
    
}

class SignUPPopUPViewController: UIViewController {
    @IBOutlet weak var messageView: UIView!
    
    @IBOutlet weak var popUpMessageLabel: UILabel!
    
    weak var delegate: SignUPPopUPViewControllerDelegate?
    var isShowPopUP : Bool = false
    var isFromEmail:Bool = true
   
    override func viewDidLoad() {
        super.viewDidLoad()
         isShowPopUP = true
        // Do any additional setup after loading the view.
        self.messageView.layer.cornerRadius = 8.0
       self.messageView.clipsToBounds = true
        
        if isFromEmail == true {
            self.popUpMessageLabel.text = "Email id already exists."
        }else{
            self.popUpMessageLabel.text = "Your Account has been submitted for approval"
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.messageView.layer.cornerRadius = 8.0
//        self.messageView.clipsToBounds = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        print("abc")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
       super.viewDidDisappear(true)
        if isShowPopUP == false{
            return
        }
        isShowPopUP = false
        print("xyz")

        self.delegate?.backgroundClickedOnPopUP(isEmail: self.isFromEmail)

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
