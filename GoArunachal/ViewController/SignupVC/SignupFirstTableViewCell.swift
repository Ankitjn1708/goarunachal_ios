//
//  SignupFirstTableViewCell.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/15/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
protocol SignupFirstTableViewCellDelegate: class {
    func LoginButtonClicked()
    
}
class SignupFirstTableViewCell: UITableViewCell {
    @IBAction func loginClicked(_ sender: Any) {
        self.delegate?.LoginButtonClicked()
    }
    weak var delegate: SignupFirstTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
