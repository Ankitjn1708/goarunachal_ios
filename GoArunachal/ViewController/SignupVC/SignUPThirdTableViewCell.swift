//
//  SignUPThirdTableViewCell.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/16/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
protocol SignUPThirdTableViewCellDelegate: class {
    func RegisterButtonClicked()
    
}
class SignUPThirdTableViewCell: UITableViewCell {
    @IBOutlet weak var signUpBtn: UIButton!
    @IBAction func signUpBtnClicked(_ sender: Any) {
        self.delegate?.RegisterButtonClicked()
    }
    weak var delegate: SignUPThirdTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ModalController.roundView(viw : self.signUpBtn)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
