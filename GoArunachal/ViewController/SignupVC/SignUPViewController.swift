//
//  SignUPViewController.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/14/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
//import LSDialogViewController
class SignUPViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,SignupFirstTableViewCellDelegate,SignUPThirdTableViewCellDelegate,SignupSecondTableViewCellDelegate,SignUPPopUPViewControllerDelegate {
    @IBOutlet weak var backbtn: UIButton!
    @IBAction func backbtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    var signupName: String? = ""
    var signupPassword: String? = ""
    var signupConfirmPassword: String? = ""
    var signupPhoneNo: String? = ""
    var signupEmail: String? = ""
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableView.automaticDimension;
        self.tableView.estimatedRowHeight = 54;
        
        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: "SignupFirstTableViewCell", bundle: nil), forCellReuseIdentifier: "SignupFirstTableViewCell");
        tableView.register(UINib(nibName: "SignupSecondTableViewCell", bundle: nil), forCellReuseIdentifier: "SignupSecondTableViewCell");
        tableView.register(UINib(nibName: "SignUPThirdTableViewCell", bundle: nil), forCellReuseIdentifier: "SignUPThirdTableViewCell");
        self.tableView.separatorStyle = .none
        self.navigationItem.setHidesBackButton(true, animated:true);
        
//        self.automaticallyAdjustsScrollViewInsets = false
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override func viewDidLayoutSubviews() {
        self.tableView.reloadData()

    }
    override func viewDidAppear(_ animated: Bool) {

    }
    //MARK:- UITableView Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignupFirstTableViewCell",for: indexPath) as! SignupFirstTableViewCell
            cell.selectionStyle = .none
            
            
            cell.tag=indexPath.row
            cell.delegate = self
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignupSecondTableViewCell",for: indexPath) as! SignupSecondTableViewCell
            cell.selectionStyle = .none
            ModalController.roundView(viw : cell.entryFieldTxtFld)
            
            cell.entryFieldTxtFld.attributedPlaceholder = NSAttributedString(string: "Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 181.0/255.0, green: 181.0/255.0, blue: 181.0/255.0, alpha: 1.0)])
            cell.entryFieldTxtFld.text = signupName
            cell.tag=indexPath.row
            cell.delegate = self
            return cell
        }else if indexPath.row == 2 {
             let cell = tableView.dequeueReusableCell(withIdentifier: "SignupSecondTableViewCell",for: indexPath) as! SignupSecondTableViewCell
            cell.selectionStyle = .none
               ModalController.roundView(viw : cell.entryFieldTxtFld)
            cell.entryFieldTxtFld.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 181.0/255.0, green: 181.0/255.0, blue: 181.0/255.0, alpha: 1.0)])
            cell.entryFieldTxtFld.text = signupEmail
            cell.delegate = self
            cell.tag=indexPath.row
            return cell
        }else if indexPath.row == 3 {
             let cell = tableView.dequeueReusableCell(withIdentifier: "SignupSecondTableViewCell",for: indexPath) as! SignupSecondTableViewCell
            cell.selectionStyle = .none
               ModalController.roundView(viw : cell.entryFieldTxtFld)
            cell.entryFieldTxtFld.attributedPlaceholder = NSAttributedString(string: "Mobile No.", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 181.0/255.0, green: 181.0/255.0, blue: 181.0/255.0, alpha: 1.0)])
            
            cell.entryFieldTxtFld.keyboardType = .phonePad
            cell.entryFieldTxtFld.text = signupPhoneNo
            cell.delegate = self
            cell.tag = indexPath.row
            return cell

        }else if indexPath.row == 4 {
             let cell = tableView.dequeueReusableCell(withIdentifier: "SignupSecondTableViewCell",for: indexPath) as! SignupSecondTableViewCell
            cell.selectionStyle = .none
               ModalController.roundView(viw : cell.entryFieldTxtFld)
            cell.entryFieldTxtFld.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 181.0/255.0, green: 181.0/255.0, blue: 181.0/255.0, alpha: 1.0)])
            cell.entryFieldTxtFld.text = signupPassword
            cell.entryFieldTxtFld.isSecureTextEntry = true

            cell.delegate = self
            cell.tag = indexPath.row
            return cell

        }else if indexPath.row == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignupSecondTableViewCell",for: indexPath) as! SignupSecondTableViewCell
            cell.selectionStyle = .none
               ModalController.roundView(viw : cell.entryFieldTxtFld)
            cell.entryFieldTxtFld.attributedPlaceholder = NSAttributedString(string: "Confirm Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 181.0/255.0, green: 181.0/255.0, blue: 181.0/255.0, alpha: 1.0)])
            cell.entryFieldTxtFld.text = signupConfirmPassword
            cell.entryFieldTxtFld.isSecureTextEntry = true
            
            cell.delegate = self
            cell.tag = indexPath.row
            return cell
            
        }
        else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "SignUPThirdTableViewCell",for: indexPath) as! SignUPThirdTableViewCell
        cell.selectionStyle = .none
            ModalController.roundView(viw : cell.signUpBtn)

            cell.tag = indexPath.row
            cell.delegate = self
            return cell

        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 350
        }else if indexPath.row == 1{
            
            return UITableView.automaticDimension
            
        }else if indexPath.row == 2{
            return UITableView.automaticDimension
        }else if indexPath.row == 3{
            return UITableView.automaticDimension
        }else if indexPath.row == 4{
            return UITableView.automaticDimension
        }else if indexPath.row == 5{
            return UITableView.automaticDimension
        }else{
            return UITableView.automaticDimension
        }
        
    }
    
    func LoginButtonClicked()
    {
     self.navigationController?.popViewController(animated: true)
        
    }
    
    func RegisterButtonClicked(){
        view.endEditing(true)

        if (signupName?.isEmpty)! || (signupEmail?.isEmpty)! || (signupPhoneNo?.isEmpty)! || (signupPassword?.isEmpty)! || (signupConfirmPassword?.isEmpty)!  {
            ModalController.showNegativeCustomAlertWith(title: "Please enter all fields.", msg: "")
        }
        else if !ModalController.isValidEmail(testStr: signupEmail!){
            ModalController.showNegativeCustomAlertWith(title: "Please enter valid email id.", msg: "")
        }
        else if ((signupPhoneNo?.count)! < 10) {
            ModalController.showNegativeCustomAlertWith(title: "Please enter a valid phone number.", msg: "")
            
        }else if ((signupPhoneNo?.count)! > 12) {
            ModalController.showNegativeCustomAlertWith(title: "Please enter a valid phone number.", msg: "")
            
        }else if (signupPassword?.count)! < 4 && (signupConfirmPassword?.count)! < 4 {
            ModalController.showNegativeCustomAlertWith(title: "", msg: "password should have minimum 4 characters.")
            
        }
        else if signupPassword != signupConfirmPassword{
            
             ModalController.showNegativeCustomAlertWith(title: "Password and confirm password are not match.", msg: "")
        }
        
        else{
         self.registerAPI()
        }
        
    }
    func entryNameSignup(entryNameStr: String, _ sender: Any)
    {
        if (sender as AnyObject).tag == 1{
            print ("1")
            signupName = entryNameStr
            print("name",entryNameStr)
        }
        else if (sender as AnyObject).tag == 2{
            print ("2")
            signupEmail = entryNameStr
            print("email",entryNameStr)

        }
        else if (sender as AnyObject).tag == 3{
            print ("3")
            signupPhoneNo = entryNameStr
            print("phoneno",entryNameStr)

            
        }else if (sender as AnyObject).tag == 4{
            signupPassword = entryNameStr
            print("password",entryNameStr)

            
        }else if (sender as AnyObject).tag == 5{
            signupConfirmPassword = entryNameStr
            print("confirmpassword",entryNameStr)

            
        }
    }
    
    func registerAPI(){
        
        ModalClass.startLoading(self.view)
        let str = "\(Constant.BASE_URL)registration.php"
        
       
        let parasm : String = "name=\(signupName!)&email=\(signupEmail!)&mobile=\(signupPhoneNo!)&password=\(signupPassword!)"
        print("request -",parasm)
        
        WebModel.makePostRequest(str, withParameters: parasm) { (response, success) in
            ModalClass.stopLoading()
            //            }
            if success == true {
                print (response ?? "N/A")
                let ResponseDict : NSDictionary = (response as? NSDictionary)!
                let x = ResponseDict.object(forKey: "status") as! NSNumber
                if !(x == 1)
                {
                    ModalController.showNegativeCustomAlertWith(title: "Sorry this email or mobile has been used, Please choose another email or mobile", msg: "")

                }
                else{

                    ModalController.showSuccessCustomAlertWith(title: "You are register sucessfully.", msg: "")
                    
                    ModalController.saveTheContent(ResponseDict.object(forKey: "UserId") as! NSNumber, WithKey: "userID")
                    ModalController.saveTheContent(ResponseDict as? AnyObject, WithKey: Constant.SavedUserData)
                    
                     NotificationCenter.default.post(name: Notification.Name("Login"), object: nil)
                    self.dismiss(animated: true, completion: nil)
                    
                }
            }else{
                if response == nil{
                    print ("connection error")
                    ModalController.showNegativeCustomAlertWith(title: "connection error", msg: "")
                    
                }else{
                    print ("data not in proper json")
                }
            }
        }
    }
    func backgroundClickedOnPopUP(isEmail:Bool)
    {
        if isEmail == false {
//            self.saveFirebaseToServer()
        }
    }
}
