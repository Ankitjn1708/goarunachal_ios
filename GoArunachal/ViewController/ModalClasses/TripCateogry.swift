//
//  TripCateogry.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/27/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit

class TripCateogry: NSObject {
    
    var tripCateogryTotalCost : String = ""
    var tripCateogryID : String = ""
    var tripCateogryPackageName : String = ""
    var tripCateogryPriceKM : String = ""
    
    init(dict : NSDictionary) {
        super.init()
        var totalCost = ""
        if let result =  dict["TotalCost"] as? String
        {
            totalCost = result
        }
        if let result =  dict["TotalCost"] as? NSNumber        {
            totalCost = String(describing: result)
        }
        self.tripCateogryTotalCost = totalCost
        
        
        var tripID = ""
        if let result =  dict["id"] as? String
        {
            tripID = result
        }
        if let result =  dict["id"] as? NSNumber        {
            tripID = String(describing: result)
        }
        self.tripCateogryID = tripID
        
        
        self.tripCateogryPackageName =  dict.object(forKey: "package") as! String
        
        var tripPrice = ""
        if let result =  dict["priceperkm"] as? String
        {
            tripPrice = result
        }
        if let result =  dict["priceperkm"] as? NSNumber        {
            tripPrice = String(describing: result)
        }
        self.tripCateogryPriceKM = tripPrice
        
    }
    override init() {
        super.init()
    }
}
