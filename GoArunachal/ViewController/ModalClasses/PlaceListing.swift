//
//  PlaceListing.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/15/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit

class PlaceListing: NSObject {
    
    var placeImage : String = ""
    var placeName : String = ""
    var placeID : String = ""
//    var place : String = ""

    init(dict : NSDictionary) {
        super.init()
//        self.placeImage = dict["imageurl"] as! String
        self.placeName = dict ["name"] as! String
        self.placeImage = ModalController.handleNull(value: dict.object(forKey: "imageurl") as AnyObject)!
        self.placeID = dict ["id"] as! String

    }
    override init() {
        super.init()
    }
  
}
