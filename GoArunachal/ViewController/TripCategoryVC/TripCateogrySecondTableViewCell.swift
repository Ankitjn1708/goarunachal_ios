//
//  TripCateogrySecondTableViewCell.swift
//  GoArunachal
//
//  Created by Ankit  Jain on 24/10/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
protocol TripCateogrySecondTableViewCellDelegate: class {
    func SelectRideTypeClicked(PlaceNameStr: TripCateogrySecondTableViewCell)
    
}
class TripCateogrySecondTableViewCell: UITableViewCell {
    @IBOutlet weak var rideTypeImgView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBAction func selectBtnClicked(_ sender: Any) {
        self.delegate?.SelectRideTypeClicked(PlaceNameStr: self)
    }
    weak var delegate: TripCateogrySecondTableViewCellDelegate?

    @IBOutlet weak var rupeeIcon: UIImageView!
    @IBOutlet weak var typeNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
