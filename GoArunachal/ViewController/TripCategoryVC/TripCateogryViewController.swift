//
//  TripCateogryViewController.swift
//  GoArunachal
//
//  Created by Ankit  Jain on 24/10/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit

class TripCateogryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,TripCateogrySecondTableViewCellDelegate,TripCateogryThirdTableViewCellDelegate {
   
    @IBOutlet weak var tableView: UITableView!
    var packageListingAPIStatus : String = ""
    var packageListArray = NSArray()
    var allPackageArray = [TripCateogry]()
    var FinalApiResponseDict : NSDictionary = NSDictionary()
    var tripCateogry : TripCateogry = TripCateogry()

    var tripModal : TripModal = TripModal()
    override func viewDidLoad() {
        super.viewDidLoad()
   self.makeNavigationButton()
        // Do any additional setup after loading the view.
        self.tableView.isHidden = true;
        tableView.register(UINib(nibName: "TripCateogryTableViewCell", bundle: nil), forCellReuseIdentifier: "TripCateogryTableViewCell");
        tableView.register(UINib(nibName: "TripCateogrySecondTableViewCell", bundle: nil), forCellReuseIdentifier: "TripCateogrySecondTableViewCell");
        tableView.register(UINib(nibName: "TripCateogryThirdTableViewCell", bundle: nil), forCellReuseIdentifier: "TripCateogryThirdTableViewCell");
        self.tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = 82;
        self .ChooseRideTypeAPI()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func makeNavigationButton()
    {
        navigationController?.navigationBar.barTintColor = Constant.APPCOLOUR
        
        self.navigationItem.title = "Choose Type"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let btn = UIButton(type: .custom)
        
        btn.widthAnchor.constraint(equalToConstant: 12).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        btn.setImage(UIImage(named: "back_icon.png"), for: .normal)
        
        btn.addTarget(self, action: #selector(backBtnClicked(btn:)), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btn)
        self.navigationItem.leftBarButtonItem = item
    }
    @objc func backBtnClicked(btn : UIButton)
    {
        _ = navigationController?.popViewController(animated: true)
        //        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
        //        return
    }
    
    // MARK:- UITableViewDelegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else if section == 1{
        return allPackageArray.count
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TripCateogryTableViewCell",for: indexPath) as! TripCateogryTableViewCell
            cell.selectionStyle = .none
            cell.toLabel.text = self.tripModal.sourceLocation.placeName;
            cell.fromLabel.text = self.tripModal.destinationLocation.placeName;
            if allPackageArray.count > 0 {
                cell.priceLabel.text = self.FinalApiResponseDict.object(forKey: "Distance") as? String
            }
            
            return cell
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TripCateogrySecondTableViewCell",for: indexPath) as! TripCateogrySecondTableViewCell
            cell.selectionStyle = .none
            
            if allPackageArray.count > 0 {
                let cateogryList = allPackageArray[indexPath.row]
                if cateogryList.tripCateogryPackageName == "Economy" {
                   cell.rideTypeImgView.image = UIImage(named: "economy_car_img.png")
                }else if cateogryList.tripCateogryPackageName == "Standard"{
                    cell.rideTypeImgView.image = UIImage(named: "standard_car_img.png")
                }else if cateogryList.tripCateogryPackageName == "Premium" {
                    cell.rideTypeImgView.image = UIImage(named: "premium_car_img.png")
                }
                cell.typeNameLabel.text = cateogryList.tripCateogryPackageName
                cell.priceLabel.text = cateogryList.tripCateogryTotalCost
            }
            cell.tag = indexPath.row
            cell.delegate = self
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TripCateogryThirdTableViewCell",for: indexPath) as! TripCateogryThirdTableViewCell
            cell.selectionStyle = .none
            
            cell.tag = indexPath.row
            cell.delegate = self
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
        return UITableView.automaticDimension
        }else if indexPath.section == 1 {
            return 145
        }
        else{
            return 80
        }
    }
    func ChooseRideTypeAPI(){
        
        ModalClass.startLoading(self.view)
        let str = "\(Constant.BASE_URL)showpackages.php"
        
        let parasm : String = "toid=\(self.tripModal.sourceLocation.placeID)&fromid=\(self.tripModal.destinationLocation.placeID)"
        print("request -",parasm)
        
        WebModel.makePostRequest(str, withParameters: parasm) { (response, success) in
            ModalClass.stopLoading()
            //            }
            if success == true {
                print (response ?? "N/A")
                
                self.FinalApiResponseDict = (response as? NSDictionary)!
                
                if let result =  (self.FinalApiResponseDict.object(forKey: "status") as? String)
                {
                    self.packageListingAPIStatus = result
                }
                if let result =  (self.FinalApiResponseDict.object(forKey: "status") as? NSNumber)        {
                    self.packageListingAPIStatus = String(describing: result)
                }
                
                let x = self.packageListingAPIStatus
                if !(x == "1")
                {
                    ModalController.showNegativeCustomAlertWith(title: self.FinalApiResponseDict.object(forKey: "message") as! String, msg: "")
                }
                else{
                    self.tableView.isHidden = false;
                   self.packageListArray = self.FinalApiResponseDict.object(forKey: "Package") as! NSArray
                    for placeList in self.packageListArray{
                        let dict = placeList as! NSDictionary
                        let placeListModal = TripCateogry(dict : dict)
                        self.allPackageArray.append(placeListModal)
                    }
                    self.tableView.reloadData()
                }
            }else{
                if response == nil
                {
                    print ("connection error")
                    
                    ModalController.showNegativeCustomAlertWith(title: "connection error", msg: "")
                    
                }else{
                    print ("data not in proper json")
                }
            }
        }
    }
    func SelectRideTypeClicked(PlaceNameStr: TripCateogrySecondTableViewCell)
 {
        let newViewController = ConfirmRideViewController()
        newViewController.tripModal = self.tripModal;
    if allPackageArray.count > 0 {
        newViewController.TotalPrice = self.FinalApiResponseDict.object(forKey: "Distance") as! String
        newViewController.estimatedTime = self.FinalApiResponseDict.object(forKey: "Time") as! String
    }
    let placeModal = allPackageArray[PlaceNameStr.tag]
        newViewController.ConfirmRideModal = placeModal
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    func ReplanRideClicked()
    {
         NotificationCenter.default.post(name: Notification.Name("replanRide"), object: nil)
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
