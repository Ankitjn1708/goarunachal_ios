//
//  TripCateogryThirdTableViewCell.swift
//  GoArunachal
//
//  Created by Ankit  Jain on 24/10/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
protocol TripCateogryThirdTableViewCellDelegate: class {
    func ReplanRideClicked()
    
}

class TripCateogryThirdTableViewCell: UITableViewCell {
    weak var delegate: TripCateogryThirdTableViewCellDelegate?
    @IBAction func replanRideClicked(_ sender: Any) {
        self.delegate?.ReplanRideClicked()
    }
    
    @IBOutlet weak var replanRideBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
