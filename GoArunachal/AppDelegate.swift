//
//  AppDelegate.swift
//  GoArunachal
//
//  Created by Neeraj Tiwari on 10/8/18.
//  Copyright © 2018 GoArunachal. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import AWSCore
import AWSS3
//#import <AWSCore/AWSCore.h>
//#import <AWSCognito/AWSCognito.h>
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var nav = UINavigationController()
    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let vc = HomeViewController(nibName: "HomeViewController", bundle: nil)
        nav = UINavigationController.init(rootViewController: vc)
        
        IQKeyboardManager.shared.enable = true
        self.window!.rootViewController = nav
        self.window!.makeKeyAndVisible()
        nav.setNavigationBarHidden(false, animated: false)
//        UIApplication.shared.statusBarStyle = .lightContent
        self.registerAWS()

        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: true)

        return true
    }
    
    
    
    
    
    // MARK: - Register AWS
    func registerAWS(){

        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.APSouth1,
                                                                identityPoolId:"ap-south-1:8ffbaef5-143f-4fa3-bfa7-d15c1a6cd22b")
        
        let configuration = AWSServiceConfiguration(region:.APSouth1, credentialsProvider:credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        
//        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.APSouth1,
//                                                                identityPoolId:"ap-south-1:120c3467-2eb0-42eb-80d6-a80f6c265149")
//        
//        let configuration = AWSServiceConfiguration(region:.APSouth1, credentialsProvider:credentialsProvider)
//        
//        AWSServiceManager.default().defaultServiceConfiguration = configuration
//        
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

