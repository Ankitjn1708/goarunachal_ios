//
//  ModalClass.swift
//  Silent Panda
//
//  Created by Neeraj Tiwari on 10/5/17.
//  Copyright © 2017 aishwarya sanganan. All rights reserved.
//

import UIKit

class ModalController: NSObject {
    
    
    static func showAlertWith(message: String, title: String )
    {
         UIAlertView.init(title: title, message: message , delegate: nil, cancelButtonTitle: "OK").show();
    }
    
    static  func base64Decoded(_ decoString:String) -> String
    {
        let decodedData = Data(base64Encoded: decoString, options:NSData.Base64DecodingOptions(rawValue: 0))
        if let decode = decodedData{
            let decodedString = NSString(data: decode, encoding: String.Encoding.utf8.rawValue)
            
            if decodedString == nil {
                return decoString;
            }
            print("decode string \n",decodedString! );
            return decodedString! as String;
        }
        else
        {
            return decoString;
        }
    }
    
    // MARK: Appdelegate Object
    static func APPDELEGATE() -> AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate;
    }

   static func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    //MARK: NSUserDefaultMethods
    static func saveTheContent(_ content : AnyObject?, WithKey key : String )-> Void {
        let defaults = UserDefaults.standard
        defaults.set(content, forKey: key as String)
        defaults.synchronize();
    }
    
///Users/aishwaryasanganan/Documents/projects/SceneAhead_ios/Scene Ahead/Scene Ahead/Scene Ahead.entitlements
    
    
    static func removeTheContentForKey(_ key : String )-> Void {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: key as String)
    }
    static func getTheContentForKey(_ key : String)-> AnyObject? {
        let defaults = UserDefaults.standard
        let value = defaults.object(forKey: key as String)
        return value as AnyObject
    }
    static func checkStringIsValid(_ str : NSString)-> Bool {
        if (str .isKind(of: NSNull .classForCoder()) || str .trimmingCharacters(in: CharacterSet.whitespaces).isEmpty){
            return false
        }
        return true
    }
   static func handleNull(value : AnyObject?) -> String? {
        if value is NSNull || value == nil {
            return ""
        } else {
            return value as? String
        }
    }
    static func roundView(viw : UIView)
    {
        viw.layer.cornerRadius = viw.frame.size.height / 2;
        viw.clipsToBounds = true;
    }
    
    static func showSuccessCustomAlertWith(title: String, msg: String)
    {
        let banner = Banner(title: title, subtitle: msg, image: UIImage(named: ""), backgroundColor: UIColor(red:78.00/255.0, green:138.0/255.0, blue:30.0/255.0, alpha:1.000))
        banner.dismissesOnTap = true
        banner.show(duration: 1.5)
    }
    
    static func showNegativeCustomAlertWith(title: String, msg: String)
    {
        let banner = Banner(title: title, subtitle: msg, image: UIImage(named: ""), backgroundColor: UIColor.red)
        banner.dismissesOnTap = true
        banner.show(duration: 1.5)
    }
    
    //Mark: CheckUserLogin
    static func isUserLoggedIn() -> Bool
    {
        if (ModalController.getTheContentForKey(Constant.SavedUserData) == nil || ModalController.getTheContentForKey(Constant.SavedUserData) is NSNull){
            return false
        }else{
            return true

        }
    }
}
