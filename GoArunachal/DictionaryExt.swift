//
//  DictionaryExt.swift
//  BaseProjectSwift
//
//  Created by Gaurav Sinha on 2/1/17.
//  Copyright © 2017 Sufalam Technologies. All rights reserved.
//

import Foundation
extension NSDictionary
{
    func RemoveNullValueFromDic()-> NSDictionary
    {
        
        let mutableDictionary:NSMutableDictionary = NSMutableDictionary(dictionary: self)
//        print("comingdict", mutableDictionary)
        for key in mutableDictionary.allKeys
        {
            if("\(mutableDictionary.object(forKey: "\(key)")!)" == "<null>" || "\(mutableDictionary.object(forKey: "\(key)")!)" is NSNull )
            {
                mutableDictionary.setValue("", forKey: key as! String)
                
//                print("outdict", mutableDictionary)
            }else if (mutableDictionary.object(forKey:key as? String)) is NSDictionary
            {
                var tempDict = (mutableDictionary.object(forKey:key as? String)) as? NSDictionary
                tempDict = tempDict?.RemoveNullValueFromDic()
                mutableDictionary.setValue(tempDict, forKey: key as! String)
                
            }
        }
        return mutableDictionary
//        print("returndict", mutableDictionary)

    }
}
